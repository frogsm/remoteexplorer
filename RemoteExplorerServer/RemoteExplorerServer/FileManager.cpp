#include "stdafx.h"
#include "FileManager.h"
#include "resource.h"
#include "TypeConvert.h"

CFileManager::CFileManager(void)
{
}


CFileManager::~CFileManager(void)
{
}

void CFileManager::CreateChildDirInfo(CString path) {
    SearchChildDirectory(path);
    int childSize = childDirPathList_.size();

    for(int index = 0; index < childSize; index++) {
        CString childPath = childDirPathList_[index];

        CString name = GetChildDirName(childPath);
        bool isDescendentZero = GetIsDescendentZero(childPath);

        CChildDirInfo* childDirInfo = new CChildDirInfo(name, isDescendentZero);
        childDirInfoList_.push_back(childDirInfo);
    }
}

std::vector<CChildDirInfo*> CFileManager::GetChildDirInfoList(void) {
    return childDirInfoList_;
}

void CFileManager::SearchChildDirectory(CString path) {
    
    CFileFind fileFind;
    BOOL findFlag = fileFind.FindFile(path + _T("*.*"));
    while(findFlag) {
        findFlag = fileFind.FindNextFile();
        if(fileFind.IsDirectory() && !fileFind.IsHidden() && !fileFind.IsSystem() && !fileFind.IsDots()) {
            CString childPath = fileFind.GetFilePath();
            childDirPathList_.push_back(childPath);
        }
    }
}

void CFileManager::CreateDirInfo(CString path) {
    SearchDirectory(path);
    int itemSize = itemPathList_.size();

    for(int index = 0; index < itemSize; index++) {
        CString itemPath = itemPathList_[index];

        CString name = GetItemName(itemPath);
        CString extension = GetItemExtension(itemPath);
        unsigned long long size = GetItemSize(itemPath);
        unsigned long long updateDate = GetItemUpdateDate(itemPath);

        CDirInfo* dirInfo = new CDirInfo(name, extension, size, updateDate);
        dirInfoList_.push_back(dirInfo);
    }

}

std::vector<CDirInfo*> CFileManager::GetDirInfoList(void) {
    return dirInfoList_;
}


bool CFileManager::IsDescendentZero(CString path) {
    CFileFind fileFind;
    BOOL findCheck = fileFind.FindFile(path + _T("\\*.*"));
    while(findCheck) {
        findCheck = fileFind.FindNextFile();
        if(fileFind.IsDirectory() && !fileFind.IsHidden() 
            && !fileFind.IsSystem() && !fileFind.IsDots()) {
            return false;
        }
    }
    return true;
}

CString CFileManager::GetChildDirName(CString path) {
    SHFILEINFO sfi;
    ZeroMemory(&sfi, sizeof(SHFILEINFO));
    SHGetFileInfo(path, 0, &sfi, sizeof(sfi), SHGFI_DISPLAYNAME | SHGFI_SYSICONINDEX);
    if(IsUserDir(sfi.szDisplayName)) {
        CTypeConvert typeConvert;
        return typeConvert.IdToCString(ID_DIR_USER_ENG);
    }

    return sfi.szDisplayName;
}

bool CFileManager::GetIsDescendentZero(CString path) {
    return IsDescendentZero(path);
}

void CFileManager::SearchDirectory(CString path) {
    CFileFind fileFind;
    BOOL findFlag = fileFind.FindFile(path + _T("*.*"));
    while(findFlag) {
        findFlag = fileFind.FindNextFile();
        if(!fileFind.IsHidden() && !fileFind.IsDots()) {
            CString itemPath = fileFind.GetFilePath();
            itemPathList_.push_back(itemPath);
        }
    }
}

CString CFileManager::GetItemName(CString path) {
    CFileFind fileFind;
    fileFind.FindFile(path);
    fileFind.FindNextFile();

    if(fileFind.IsDirectory()) {
        SHFILEINFO sfi;
        ZeroMemory(&sfi, sizeof(SHFILEINFO));
        SHGetFileInfo(path, 0, &sfi, sizeof(sfi), SHGFI_DISPLAYNAME | SHGFI_SYSICONINDEX);
        if(IsUserDir(sfi.szDisplayName)) {
            CTypeConvert typeConvert;
            return typeConvert.IdToCString(ID_DIR_USER_ENG);
        }
        return sfi.szDisplayName;
    } 
    int separatorPos = fileFind.GetFileName().ReverseFind('.');

    return fileFind.GetFileName().Left(separatorPos);
}

CString CFileManager::GetItemExtension(CString path) {
    CFileFind fileFind;
    fileFind.FindFile(path);
    fileFind.FindNextFile();

    if(!fileFind.IsDirectory()) {
        // 확장자 분리 하기.
        CString copyPath = path;
        int separatorPos = path.ReverseFind('.');
        return path.Right((path.GetLength() - separatorPos) - 1);
    }
    return CString(_T(" "));
}

unsigned long long CFileManager::GetItemSize(CString path) {
    CFileFind fileFind;
    fileFind.FindFile(path);
    fileFind.FindNextFile();

    CFileStatus status;
    CFile::GetStatus(path, status);

    if(fileFind.IsDirectory()) {
        return 0;
    }

    return status.m_size;
}

unsigned long long CFileManager::GetItemUpdateDate(CString path) {
    CFileStatus status;
    CFile::GetStatus(path, status);
    
    CString ctimeToCStr = status.m_mtime.Format(_T("%Y%m%d%H%M"));
    return _tcstoui64(ctimeToCStr, NULL, 10);
}

bool CFileManager::IsUserDir(CString name) {
    CTypeConvert typeConvert;
    CString usersDirName = typeConvert.IdToCString(ID_DIR_USER);

    return _tcscmp(name, usersDirName) == 0 ? true : false;
}
