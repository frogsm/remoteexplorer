﻿
// RemoteExplorerServerDlg.h : 헤더 파일
//

#ifndef REMOTEEXPLORERSERVERDLG_H
#define REMOTEEXPLORERSERVERDLG_H

#include "afxwin.h"
#include "resource.h"
#include "SocketManager.h"
#include "SvcCtrlManager.h"

// CRemoteExplorerServerDlg 대화 상자
class CRemoteExplorerServerDlg : public CDialogEx
{
// 생성입니다.
public:
	CRemoteExplorerServerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	virtual ~CRemoteExplorerServerDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_REMOTEEXPLORERSERVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

// ## 사용자 데이터 추가
private:
    CSocketManager* socketManager_;
    CSvcCtrlManager* svcCtrlManager_;

	CString recvAllMsg_;			// ## 받은 메세지를 누적으로 출력해주는 변수.

	CEdit ctrlMsgInfo_;		    	// ## 받은 메세지 출력하는 컨트롤 변수
	CString valMsgInfo_;			// ## 위의 변수에 대한 Value형 변수.

	CButton ctrlServerStart_;	    // ## 서버 시작버튼 컨트롤 변수
	CButton ctrlServerExit_;		// ## 서버 종료버튼 컨트롤 변수

    CButton ctrlAutoStart_;
    BOOL valAutoStart_;             // ## 윈도우 부팅시 자동시작 Value형 변수

public:
    CSocketManager* GetSocketManager(void) const;

	// ## 서버 시작버튼
	afx_msg void OnBnClickedServerstart();
	// ## 서버 종료버튼
	afx_msg void OnBnClickedServerexit();

	// ## 클라이언트 연결
	void ConnectClient(void);
	// ## 클라이언트 연결 해제
	void DisconnectClient(void);

    // ## 소켓에서 받은 메세지를 출력해주는 부분.
    afx_msg LRESULT OnShowMessage(WPARAM wparam, LPARAM lparam);
    
    afx_msg void OnBnClickedChkAutostart();
    
};

#endif /*REMOTEEXPLORERSERVERDLG_H*/