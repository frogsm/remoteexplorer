#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "ChildDirInfo.h"
#include "DirInfo.h"
#include <vector>

class CFileManager
{
private:
    std::vector<CChildDirInfo*> childDirInfoList_;
    std::vector<CString> childDirPathList_;

    std::vector<CDirInfo*> dirInfoList_;
    std::vector<CString> itemPathList_;

public:
    CFileManager(void);
    virtual ~CFileManager(void);

    void CreateChildDirInfo(CString path);
    std::vector<CChildDirInfo*> GetChildDirInfoList(void);

    void CreateDirInfo(CString path);
    std::vector<CDirInfo*> GetDirInfoList(void);

private:
    void SearchChildDirectory(CString path);
    bool IsDescendentZero(CString path);
    CString GetChildDirName(CString path);
    bool GetIsDescendentZero(CString path);

    void SearchDirectory(CString path);
    CString GetItemName(CString path);
    CString GetItemExtension(CString path);
    unsigned long long GetItemSize(CString path);
    unsigned long long GetItemUpdateDate(CString path);

    bool IsUserDir(CString name);
};

#endif /*FILEMANAGER_H*/