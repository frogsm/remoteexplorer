﻿#include "stdafx.h"
#include "ServerController.h"

CServerController::CServerController()
{
    driveManager_ = new CDriveManager();
    fileManager_ = new CFileManager();
}

CServerController::~CServerController()
{
    if(fileManager_ != nullptr) {
        delete fileManager_;
    }
    if(driveManager_ != nullptr) {
        delete driveManager_;
    }
}

std::vector<CDriveInfo*> CServerController::GetDriveInfoList(void) {
    std::vector<CDriveInfo*> driveInfoList;
    driveManager_->CreateDriveInfoList();
    driveInfoList = driveManager_->GetDriveInfoList();

    return driveInfoList;
}

std::vector<CChildDirInfo*> CServerController::GetChildDirInfoList(CString path) {
    std::vector<CChildDirInfo*> childDirInfoList;
    fileManager_->CreateChildDirInfo(path);
    childDirInfoList = fileManager_->GetChildDirInfoList();

    return childDirInfoList;
}

std::vector<CDirInfo*> CServerController::GetDirInfoList(CString path) {
    std::vector<CDirInfo*> dirInfoList;
    fileManager_->CreateDirInfo(path);
    dirInfoList = fileManager_->GetDirInfoList();

    return dirInfoList;
}