﻿#include "stdafx.h"
#include "SvcCtrlManager.h"
#include "Resource.h"
#include "TypeConvert.h"

CSvcCtrlManager::CSvcCtrlManager()
{
    CTypeConvert typeConvert;
    svcPath = _T("C:\\Users\\frogsm\\Documents\\RemoteExplorer-frogsm\\RemoteExplorerServer\\Debug\\ServerService.exe");
    svcName = typeConvert.IdToCString(ID_SVC_NAME);
    svcDesc = typeConvert.IdToCString(ID_SVC_DESC);
}

CSvcCtrlManager::~CSvcCtrlManager()
{
}

void CSvcCtrlManager::InstallSvc(void)
{
    SERVICE_DESCRIPTION desc;

    scManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    svcStatusHandle = OpenService(scManager, svcName, SERVICE_ALL_ACCESS);

    if (_taccess(svcPath, 0) != 0) {
        CloseServiceHandle(scManager);
        AfxMessageBox(ID_SCM_EXIST, MB_OK);
        return;
    }

    svcStatusHandle = CreateService(
        scManager, svcName, svcName,
        SERVICE_PAUSE_CONTINUE | SERVICE_CHANGE_CONFIG,
        SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
        SERVICE_AUTO_START,
        SERVICE_ERROR_IGNORE,
        svcPath,
        NULL, NULL, NULL, NULL, NULL);

    if (svcStatusHandle == NULL) {
        AfxMessageBox(ID_SCM_INSTALL_FAIL, MB_OK);
    }
    else {
        desc.lpDescription = (LPWSTR)(LPCTSTR)svcDesc;
        ChangeServiceConfig2(svcStatusHandle, SERVICE_CONFIG_DESCRIPTION, &desc);
        AfxMessageBox(ID_SCM_INSTALL_SUCCESS, MB_OK);
    }
    CloseServiceHandle(svcStatusHandle);
    CloseServiceHandle(scManager);
}

void CSvcCtrlManager::StartSvc(void)
{
    SERVICE_STATUS sStatus;
    scManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    svcStatusHandle = OpenService(scManager, svcName, SERVICE_ALL_ACCESS);

    ControlService(svcStatusHandle, SERVICE_CONTROL_CONTINUE, &sStatus);

    CloseServiceHandle(svcStatusHandle);
    CloseServiceHandle(scManager);
}

void CSvcCtrlManager::StopSvc(void)
{
    SERVICE_STATUS sStatus;
    scManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    svcStatusHandle = OpenService(scManager, svcName, SERVICE_ALL_ACCESS);

    QueryServiceStatus(svcStatusHandle, &sStatus);
    if (sStatus.dwCurrentState != SERVICE_STOPPED) {
        ControlService(svcStatusHandle, SERVICE_CONTROL_STOP, &sStatus);
        Sleep(2000);
    }

    CloseServiceHandle(svcStatusHandle);
    CloseServiceHandle(scManager);
}

void CSvcCtrlManager::UninstallSvc(void)
{
    scManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    svcStatusHandle = OpenService(scManager, svcName, SERVICE_ALL_ACCESS);

    if (DeleteService(svcStatusHandle)) {
        AfxMessageBox(ID_SCM_UNINSTALL_SUCCESS, MB_OK);
    } else {
        AfxMessageBox(ID_SCM_UNINSTALL_FAIL, MB_OK);
    }

    CloseServiceHandle(svcStatusHandle);
    CloseServiceHandle(scManager);
}

bool CSvcCtrlManager::IsExistSvc(void)
{
    scManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    svcStatusHandle = OpenService(scManager, svcName, SERVICE_ALL_ACCESS);

    if (svcStatusHandle != NULL) {
        CloseServiceHandle(svcStatusHandle);
        CloseServiceHandle(scManager);
        return true;
    }
    return false;
}

bool CSvcCtrlManager::IsRunSvc(void)
{
    SERVICE_STATUS sStatus;
    scManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    svcStatusHandle = OpenService(scManager, svcName, SERVICE_ALL_ACCESS);

    QueryServiceStatus(svcStatusHandle, &sStatus);
    if (sStatus.dwCurrentState == SERVICE_RUNNING) {
        CloseServiceHandle(svcStatusHandle);
        CloseServiceHandle(scManager);
        return true;
    }
    CloseServiceHandle(svcStatusHandle);
    CloseServiceHandle(scManager);
    return false;
}
