﻿#ifndef JSONHELPER_H
#define JSONHELPER_H

#include "json\json.h"
#include "DriveInfo.h"
#include "ChildDirInfo.h"
#include "DirInfo.h"
#include <string>

class CJsonHelper
{
private:
    Json::Value recvJson_;
    int command_;
    std::string sendMsg_;
public:
	CJsonHelper(void);
	virtual ~CJsonHelper(void);
	void ParseJsonCommand(CString msg);
    void StartJob(void);
    std::string CreateDriveInfo(std::vector<CDriveInfo*> info);
    std::string CreateChildDirInfo(std::vector<CChildDirInfo*> info);
    std::string CreateDirInfo(std::vector<CDirInfo*> info);
    std::string GetJsonString(void);
};

#endif /*JSONHELPER_H*/