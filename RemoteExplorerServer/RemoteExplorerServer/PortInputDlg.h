﻿#pragma once


// CPortInputDlg 대화 상자입니다.

class CPortInputDlg : public CDialog
{
	DECLARE_DYNAMIC(CPortInputDlg)

public:
	CPortInputDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CPortInputDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_PORTINPUTDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

private:
    int valPortNumber_;

public:
    int GetPortNumber(void);

	DECLARE_MESSAGE_MAP()
    
    afx_msg void OnBnClickedOk();
};
