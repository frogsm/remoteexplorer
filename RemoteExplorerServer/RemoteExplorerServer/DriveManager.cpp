#include "stdafx.h"
#include "DriveManager.h"


CDriveManager::CDriveManager(void)
{
}

CDriveManager::~CDriveManager(void)
{
    int driveInfoSize = driveInfoList_.size();
    for(int i=0 ; i<driveInfoSize; i++) {
        if(driveInfoList_.at(i) != nullptr) {
            delete driveInfoList_.at(i);
        }
    }
}

void CDriveManager::CreateDriveInfoList(void) {
    SearchDrive();
    int driveCount = drivePathList_.size();
    
    for(int index = 0; index < driveCount; index++) {
        CString path = drivePathList_[index];

        CString name = GetDriveDisplayName(path);
        int type = GetDriveTypeIndex(path);
        bool isWin = GetIsWinDrive(path);
        bool isChildZero = GetIsChildZero(path);

        CDriveInfo* driveInfo = new CDriveInfo(name, type, isWin, isChildZero);
        driveInfoList_.push_back(driveInfo);
    }
}

std::vector<CDriveInfo*> CDriveManager::GetDriveInfoList(void) {
    return driveInfoList_;
}

void CDriveManager::SearchDrive(void) {
    TCHAR buffer[256];
    ZeroMemory(buffer, 256);

    int count = GetLogicalDriveStrings(256, buffer);
    for(int i=0 ; i < count/4; i++) {
        CString drive = CString(buffer+(i*4), 3);
        drivePathList_.push_back(drive);
    }
}

bool CDriveManager::IsWindowDrive(CString path) {
    CString windowPath;

    LPITEMIDLIST item = NULL;
    SHGetSpecialFolderLocation(NULL, CSIDL_WINDOWS, &item);
    SHGetPathFromIDList(item, const_cast<LPWSTR>(static_cast<LPCTSTR>(windowPath)));

    return path[0] == windowPath[0] ? true : false;
}

bool CDriveManager::IsChildZero(CString path) {
    CFileFind fileFind;
    BOOL findCheck = fileFind.FindFile(path + _T("*.*"));
    while(findCheck) {
        findCheck = fileFind.FindNextFile();
        if(fileFind.IsDirectory() && !fileFind.IsHidden() && !fileFind.IsSystem()) {
            return false;
        }
    }
    return true;
}

CString CDriveManager::GetDriveDisplayName(CString path) {
    SHFILEINFO sfi;
    ZeroMemory(&sfi, sizeof(SHFILEINFO));
    SHGetFileInfo(path, 0, &sfi, sizeof(sfi), SHGFI_DISPLAYNAME | SHGFI_SYSICONINDEX | SHGFI_TYPENAME);
    return sfi.szDisplayName;
}

int CDriveManager::GetDriveTypeIndex(CString path) {
    return GetDriveType(path);
}

bool CDriveManager::GetIsWinDrive(CString path) {
    return IsWindowDrive(path);
}

bool CDriveManager::GetIsChildZero(CString path) {
    return IsChildZero(path);
}
