﻿#ifndef SOCKETMANAGER_H
#define SOCKETMANAGER_H

#include "BaseManager.h"
#include "ListenSocket.h"
#include "ClientSocket.h"
#include "ThreadManager.h"
#include "SocketThread.h"
#include <map>

struct ClientInfo {
    CSocketThread* thread;
    CClientSocket* socket;
};

typedef std::map<DWORD, ClientInfo*> ClientList;

class CSocketManager : public CBaseManager
{
private:
    CThreadManager* threadManager_;
    CListenSocket* listenSocket_;
    ClientList acceptList_;
    ClientList exitList_;

public:
    CSocketManager(void);
    virtual ~CSocketManager(void);

    void InitSocketManager(void);
    void TerminateSocketManager(void);

    virtual void Connect(void);
    virtual void Accept(void);
    virtual void Send(std::string msg);
    virtual void Receive(CString msg);
    virtual void Close(void);

private:
    void DeleteClientList(ClientList& list);
    void SendMessageToView(int type, CString ip);
};

#endif /*SOCKETMANAGER_H*/