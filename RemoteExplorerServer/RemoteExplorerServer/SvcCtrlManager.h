﻿#ifndef SVCCTRLMANAGER_H
#define SVCCTRLMANAGER_H

#include <winsvc.h>

class CSvcCtrlManager
{
private:
	SC_HANDLE scManager;
	SC_HANDLE svcStatusHandle;
    CString svcPath;
    CString svcName;
    CString svcDesc;

public:
	CSvcCtrlManager();
	virtual ~CSvcCtrlManager();

	void InstallSvc(void);
	void StartSvc(void);
	void StopSvc(void);
	void UninstallSvc(void);

    bool IsExistSvc(void);
    bool IsRunSvc(void);
};

#endif /*SVCCTRLMANAGER_H*/