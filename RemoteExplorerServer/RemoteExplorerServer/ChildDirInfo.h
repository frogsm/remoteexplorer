#ifndef CHILDDIRINFO_H
#define CHILDDIRINFO_H

class CChildDirInfo
{
private:
    CString childName_;
    bool isDescendentZero_;

    CChildDirInfo(void);

public:
    CChildDirInfo(CString name, bool isZero);
    virtual ~CChildDirInfo(void);

    CString GetChildName(void);
    bool GetIsDescendentZero(void);
};

#endif /*CHILDDIRINFO_H*/