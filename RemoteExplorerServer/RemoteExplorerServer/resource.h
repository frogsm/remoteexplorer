//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// RemoteExplorerServer.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_REMOTEEXPLORERSERVER_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDD_PORTINPUTDLG                129
#define ID_EDIT_START                   200
#define ID_EDIT_EXIT                    201
#define ID_BTN_START                    250
#define ID_BTN_EXIT                     251
#define ID_SOCKET_INIT                  1000
#define IDC_EDIT1                       1001
#define ID_SOCKET_CREATE                1001
#define IDC_SERVERSTART                 1002
#define ID_SOCKET_LISTEN                1002
#define IDC_SERVEREXIT                  1003
#define ID_SOCKET_ACCEPT                1003
#define IDC_EDIT_PORTNUM                1004
#define ID_SOCKET_CLOSE                 1005
#define IDC_CHK_AUTOSTART               1006
#define ID_DISK_LOCAL                   1050
#define ID_DISK_CD                      1051
#define ID_DIR_USER                     1500
#define ID_DIR_USER_ENG                 1501
#define ID_ALREADY_EXISTS               1600
#define ID_SCM_SVCNAME                  1650
#define ID_SCM_AUTH                     1650
#define ID_SCM_EXIST                    1651
#define ID_SCM_SUCCESS                  1652
#define ID_SCM_INSTALL_SUCCESS          1652
#define ID_SCM_INSTALL_FAIL             1653
#define ID_SCM_UNINSTALL_SUCCESS        1654
#define ID_SCM_UNINSTALL_FAIL           1655
#define ID_SVC_NAME                     1700
#define ID_SVC_PATH                     1701
#define ID_SVC_DESC                     1702

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
