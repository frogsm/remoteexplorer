﻿#include "stdafx.h"
#include "JsonHelper.h"

#include "TypeConvert.h"
#include "Protocol.h"
#include "ServerController.h"
using namespace Json;

CJsonHelper::CJsonHelper()
{
}

CJsonHelper::~CJsonHelper()
{
}

void CJsonHelper::ParseJsonCommand(CString msg)
{
    CTypeConvert typeConvert;

    // ## JSON 관련 변수.
    Reader reader;
    std::string recvMsg;

    recvMsg = typeConvert.CStringToString(msg);
    reader.parse(recvMsg, recvJson_);

    command_ = recvJson_[JSON::COMMAND].asInt();
}

void CJsonHelper::StartJob(void) {
    CServerController serverController;

    switch (command_)
    {
    case MESSAGE::DRIVEINFO :
        {
            std::vector<CDriveInfo*> driveInfoList;
            driveInfoList = serverController.GetDriveInfoList();

            sendMsg_ = CreateDriveInfo(driveInfoList);
        }
        break;

    case MESSAGE::CHILDDIRINFO :
        {
            CString path(recvJson_[JSON::FULLPATH].asCString());
            std::vector<CChildDirInfo*> childDirInfoList;
            childDirInfoList = serverController.GetChildDirInfoList(path);

            sendMsg_ = CreateChildDirInfo(childDirInfoList);
        }
        break;

    case MESSAGE::DIRINFO :
        {
            CString path(recvJson_[JSON::FULLPATH].asCString());
            std::vector<CDirInfo*> dirInfoList;
            dirInfoList = serverController.GetDirInfoList(path);

            sendMsg_ = CreateDirInfo(dirInfoList);
        }
        break;

    default:
        break;
    }
}

std::string CJsonHelper::CreateDriveInfo(std::vector<CDriveInfo*> info) {
    FastWriter writer;
    Value jsonObject;
    CTypeConvert convert;
    std::string jsonMsg;

    jsonObject[JSON::COMMAND] = MESSAGE::DRIVEINFO;      
    std::vector<CDriveInfo*> driveInfoList = info;
    Value driveList;

    int listSize = driveInfoList.size();
    for(int index = 0; index < listSize; index++) {
        CDriveInfo* driveInfo = driveInfoList.at(index);
        Value drive;
        drive[JSON::NAME] = convert.CStringToString(driveInfo->GetName());
        drive[JSON::TYPE] = driveInfo->GetType();
        drive[JSON::ISWIN] = driveInfo->GetIsWin();
        drive[JSON::ISCHILDZERO] = driveInfo->GetIsChildZero();
        driveList.append(drive);
    }
    jsonObject[JSON::DRIVELIST] = driveList;
    return writer.write(jsonObject);
}

std::string CJsonHelper::CreateChildDirInfo(std::vector<CChildDirInfo*> info) {
    FastWriter writer;
    Value jsonObject;
    CTypeConvert convert;
    std::string jsonMsg;

    jsonObject[JSON::COMMAND] = MESSAGE::CHILDDIRINFO;
    std::vector<CChildDirInfo*> childDirInfoList = info;
    Value childList;

    int listSize = childDirInfoList.size();
    for(int index = 0; index < listSize; index++) {
        CChildDirInfo* childDirInfo = childDirInfoList.at(index);
        Value child;
        child[JSON::NAME] = convert.CStringToString(childDirInfo->GetChildName());
        child[JSON::ISDESCENDENTZERO] = childDirInfo->GetIsDescendentZero();
        childList.append(child);
    }
    jsonObject[JSON::CHILDLIST] = childList;
    return writer.write(jsonObject);
}

std::string CJsonHelper::CreateDirInfo(std::vector<CDirInfo*> info) {
    FastWriter writer;
    Value jsonObject;
    CTypeConvert convert;
    std::string jsonMsg;

    jsonObject[JSON::COMMAND] = MESSAGE::DIRINFO;
    std::vector<CDirInfo*> dirInfoList = info;
    Value itemInfoList;

    int listSize = dirInfoList.size();
    for(int index = 0; index < listSize; index++) {
        char buffer[100];
        CDirInfo* itemInfo = dirInfoList.at(index);
        Value item;
        item[JSON::NAME] = convert.CStringToString(itemInfo->GetName());
        item[JSON::EXTENSION] = convert.CStringToString(itemInfo->GetExtension());
        _ui64toa_s(itemInfo->GetSize(), buffer, sizeof(buffer), 10);
        item[JSON::SIZE] = buffer;
        _ui64toa_s(itemInfo->GetUpdateDate(), buffer, sizeof(buffer), 10);
        item[JSON::UPDATEDATE] = buffer;
        itemInfoList.append(item);
    }
    jsonObject[JSON::ITEMLIST] = itemInfoList;
    return writer.write(jsonObject);
}

std::string CJsonHelper::GetJsonString(void) {
    return sendMsg_;
}
