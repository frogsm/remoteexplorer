#include "stdafx.h"
#include "TypeConvert.h"


CTypeConvert::CTypeConvert(void)
{
}

CTypeConvert::~CTypeConvert(void)
{
}

CString CTypeConvert::IdToCString(int id) {
    CString str;
    str.LoadString(id);
    return str;
}

CString CTypeConvert::StringToCString(std::string str) {
    return CString(str.c_str());
}

std::string CTypeConvert::CStringToString(CString cstr) {
    CT2CA pszConvertedAnsiString(cstr);
    return std::string(pszConvertedAnsiString);
}