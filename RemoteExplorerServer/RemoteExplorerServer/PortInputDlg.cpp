﻿// PortInputDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "RemoteExplorerServer.h"
#include "PortInputDlg.h"
#include "afxdialogex.h"


// CPortInputDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPortInputDlg, CDialog)

CPortInputDlg::CPortInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPortInputDlg::IDD, pParent)
    , valPortNumber_(8080)
{
    
}

CPortInputDlg::~CPortInputDlg()
{
}

void CPortInputDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_EDIT_PORTNUM, valPortNumber_);
}


BEGIN_MESSAGE_MAP(CPortInputDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CPortInputDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CPortInputDlg 메시지 처리기입니다.


void CPortInputDlg::OnBnClickedOk()
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    UpdateData(TRUE);
    CDialog::OnOK();
}

int CPortInputDlg::GetPortNumber(void) {
    return valPortNumber_;
}

