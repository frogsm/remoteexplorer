﻿#include "stdafx.h"
#include "DriveInfo.h"

CDriveInfo::CDriveInfo(CString name, int type, bool isWin, bool isChildZero)
    : driveName_(name), driveType_(type)
    , driveIsWin_(isWin), driveIsChildZero_(isChildZero)
{
}

CDriveInfo::~CDriveInfo(void)
{
}

CString CDriveInfo::GetName(void) {
    return driveName_;
}

int CDriveInfo::GetType(void) {
    return driveType_;
}

bool CDriveInfo::GetIsWin(void) {
    return driveIsWin_;
}

bool CDriveInfo::GetIsChildZero(void) {
    return driveIsChildZero_;
}

