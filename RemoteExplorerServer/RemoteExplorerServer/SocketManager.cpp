﻿#include "stdafx.h"
#include "SocketManager.h"
#include "resource.h"
#include "JsonHelper.h"
#include "RemoteExplorerServerDlg.h"
#include "TypeConvert.h"
#include "SocketThread.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSocketManager::CSocketManager(void)
    : listenSocket_(nullptr)
{
    threadManager_ = new CThreadManager();
}

CSocketManager::~CSocketManager(void)
{
    if(acceptList_.size() != 0) {
        DeleteClientList(acceptList_);
    }
    if(exitList_.size() != 0) {
        DeleteClientList(exitList_);
    }
    if(listenSocket_ != nullptr) {
        delete listenSocket_;
    }
    if(threadManager_ != nullptr) {
        delete threadManager_;
    }
}

void CSocketManager::InitSocketManager(void) {
    listenSocket_ = new CListenSocket(this);

    if(!AfxSocketInit()) {
        AfxMessageBox(ID_SOCKET_INIT);
        AfxGetMainWnd()->PostMessage(WM_CLOSE);
    }
    if(!listenSocket_->Create(SERVER_PORT)) {
        AfxMessageBox(ID_SOCKET_CREATE);
        AfxGetMainWnd()->PostMessage(WM_CLOSE);
    }
    if(!listenSocket_->Listen()) {
        AfxMessageBox(ID_SOCKET_LISTEN);
        AfxGetMainWnd()->PostMessage(WM_CLOSE);
    }
}

void CSocketManager::TerminateSocketManager(void) {
    DeleteClientList(acceptList_);
    acceptList_.clear();

    DeleteClientList(exitList_);
    exitList_.clear();

    listenSocket_->Close();

    if(listenSocket_ != nullptr) {
        delete listenSocket_;
        listenSocket_ = nullptr;
    }
}

void CSocketManager::Connect(void) {
    return ;
}

void CSocketManager::Accept(void) {

    if (acceptList_.size() >= MAX_CONNECT) {
        return;
    }

    CClientSocket* clientSocket = new CClientSocket(this);

    sockaddr_in acceptIP = {0};
	int addrSize = sizeof(sockaddr);
    listenSocket_->Accept(*clientSocket, reinterpret_cast<sockaddr*>(&acceptIP), &addrSize);
    clientSocket->SetIpAddress(inet_ntoa(acceptIP.sin_addr));
    listenSocket_->Listen();

    CSocketThread* thread = threadManager_->CreateSocketThread(clientSocket);

    ClientInfo *clientInfo = new ClientInfo;
    clientInfo->thread = thread;
    clientInfo->socket = clientSocket;

    acceptList_.insert(std::make_pair(thread->m_nThreadID, clientInfo));

    SendMessageToView(ID_SOCKET_ACCEPT, clientSocket->GetIpAddress());
}

void CSocketManager::Send(std::string msg) {
    DWORD threadID = GetThreadId(GetCurrentThread());
    acceptList_[threadID]->socket->Send(msg.c_str(), msg.length() + 1);
}

void CSocketManager::Receive(CString msg) {
    CJsonHelper jsonHelper;
    jsonHelper.ParseJsonCommand(msg);
    jsonHelper.StartJob();
    std::string sendMsg = jsonHelper.GetJsonString();

    Send(sendMsg);
}

void CSocketManager::Close(void) {
    DWORD threadID = GetThreadId(GetCurrentThread());
    ClientInfo* clientInfo = acceptList_[threadID];
    CSocketThread* thread = clientInfo->thread;
    CClientSocket* clientSocket = clientInfo->socket;
    
    clientSocket->Close();
    threadManager_->StopThread(thread);
    exitList_.insert(std::make_pair(threadID, clientInfo));
    acceptList_.erase(threadID);

    SendMessageToView(ID_SOCKET_CLOSE, clientSocket->GetIpAddress());
}

void CSocketManager::DeleteClientList(ClientList& list) {
    ClientList::iterator itMap = list.begin();

    while(itMap != list.end()) {
        ClientInfo* clientInfo = list[itMap->first];
        CSocketThread* thread = clientInfo->thread;

        threadManager_->DeleteSocketThread(thread);

        delete clientInfo;
        ++itMap;
    }
}

void CSocketManager::SendMessageToView(int type, CString ip) {
    CRemoteExplorerServerDlg* dlg =  static_cast<CRemoteExplorerServerDlg*>(AfxGetApp()->m_pMainWnd);
    CTypeConvert typeConvert;
    CString msg = _T("");

    switch(type) {
    case ID_SOCKET_ACCEPT:
        msg = ip + typeConvert.IdToCString(ID_SOCKET_ACCEPT);
        break;

    case ID_SOCKET_CLOSE:
        msg = ip + typeConvert.IdToCString(ID_SOCKET_CLOSE);
        break;

    default:
        break;
    }

    dlg->SendMessage(UM_SHOW_MSG, NULL, (LPARAM)&msg);
}
