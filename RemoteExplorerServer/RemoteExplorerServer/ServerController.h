﻿#ifndef SERVERCONTROLLER_H
#define SERVERCONTROLLER_H

#include "DriveManager.h"
#include "FileManager.h"
#include "DriveInfo.h"
#include "ChildDirInfo.h"
#include "DirInfo.h"
#include <vector>

class CServerController
{
private:
    CDriveManager* driveManager_;
    CFileManager* fileManager_;

public:
	CServerController(void);
	virtual ~CServerController(void);
    std::vector<CDriveInfo*> GetDriveInfoList(void);
    std::vector<CChildDirInfo*> GetChildDirInfoList(CString path);
    std::vector<CDirInfo*> GetDirInfoList(CString path);
};

#endif /*SERVERCONTROLLER_H*/