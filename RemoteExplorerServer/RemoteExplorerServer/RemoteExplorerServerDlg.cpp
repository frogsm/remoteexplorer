﻿
// RemoteExplorerServerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "RemoteExplorerServer.h"
#include "RemoteExplorerServerDlg.h"
#include "afxdialogex.h"
#include "TypeConvert.h"
#include "PortInputDlg.h"
#include "Constants.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


extern int SERVER_PORT;

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CRemoteExplorerServerDlg 대화 상자



CRemoteExplorerServerDlg::CRemoteExplorerServerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRemoteExplorerServerDlg::IDD, pParent)
	, valMsgInfo_(_T("")), recvAllMsg_(_T(""))
    , valAutoStart_(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    socketManager_ = new CSocketManager();
    svcCtrlManager_ = new CSvcCtrlManager();
}

CRemoteExplorerServerDlg::~CRemoteExplorerServerDlg() {
    if(socketManager_ != nullptr) {
        delete socketManager_;
    }
    if (svcCtrlManager_ != nullptr) {
        delete svcCtrlManager_;
    }
}

void CRemoteExplorerServerDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT1, ctrlMsgInfo_);
    DDX_Control(pDX, IDC_SERVERSTART, ctrlServerStart_);
    DDX_Control(pDX, IDC_SERVEREXIT, ctrlServerExit_);
    DDX_Text(pDX, IDC_EDIT1, valMsgInfo_);
    DDX_Check(pDX, IDC_CHK_AUTOSTART, valAutoStart_);
    DDX_Control(pDX, IDC_CHK_AUTOSTART, ctrlAutoStart_);
}

BEGIN_MESSAGE_MAP(CRemoteExplorerServerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SERVERSTART, &CRemoteExplorerServerDlg::OnBnClickedServerstart)
	ON_BN_CLICKED(IDC_SERVEREXIT, &CRemoteExplorerServerDlg::OnBnClickedServerexit)
    ON_MESSAGE(UM_SHOW_MSG, OnShowMessage)
    ON_BN_CLICKED(IDC_CHK_AUTOSTART, &CRemoteExplorerServerDlg::OnBnClickedChkAutostart)
END_MESSAGE_MAP()


// CRemoteExplorerServerDlg 메시지 처리기

BOOL CRemoteExplorerServerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

    HANDLE hEvent;
    hEvent = CreateEvent(NULL, FALSE, TRUE, AfxGetAppName());
    if ( GetLastError() == ERROR_ALREADY_EXISTS) {
        AfxMessageBox(ID_ALREADY_EXISTS);
        PostQuitMessage(WM_QUIT);
    }

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
    CPortInputDlg dlg ;
    INT_PTR responseDlg = dlg.DoModal();
    
    if (svcCtrlManager_->IsExistSvc() || svcCtrlManager_->IsRunSvc()) {
        ctrlAutoStart_.SetCheck(TRUE);
    }
    else {
        ctrlAutoStart_.SetCheck(FALSE);
    }

    if(responseDlg == IDOK) {
        SERVER_PORT = dlg.GetPortNumber();
	}
	//	## Cancel 버튼
	else if(responseDlg == IDCANCEL) {
		AfxGetMainWnd() -> PostMessage(WM_CLOSE);	// ## 윈도우 종료.
	}

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CRemoteExplorerServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CRemoteExplorerServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CRemoteExplorerServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


CSocketManager* CRemoteExplorerServerDlg::GetSocketManager(void) const {
    return socketManager_;
}

// ## Start
// ## 서버 시작버튼 이벤트
void CRemoteExplorerServerDlg::OnBnClickedServerstart()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// ## Start
	ConnectClient();
	ctrlServerStart_.EnableWindow(FALSE);
	ctrlServerExit_.EnableWindow(TRUE);
    AfxMessageBox(ID_BTN_START);

    CTypeConvert typeConvert;
    recvAllMsg_ = typeConvert.IdToCString(ID_EDIT_START) + _T("\r\n");
	valMsgInfo_.SetString(recvAllMsg_);
	UpdateData(FALSE);
	// ## End
}

// ## 서버 종료버튼 이벤트
void CRemoteExplorerServerDlg::OnBnClickedServerexit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//	## Start
	DisconnectClient();
	ctrlServerStart_.EnableWindow(TRUE);
	ctrlServerExit_.EnableWindow(FALSE);
    AfxMessageBox(ID_BTN_EXIT);
	
    CTypeConvert typeConvert;
    recvAllMsg_ = typeConvert.IdToCString(ID_EDIT_EXIT) + _T("\r\n") + recvAllMsg_;
	valMsgInfo_.SetString(recvAllMsg_);
	UpdateData(FALSE);
	//	## End
}

// ## 클라이언트 연결
void CRemoteExplorerServerDlg::ConnectClient(void){ 
    socketManager_->InitSocketManager();
}

// ## 클라이언트 해제
void CRemoteExplorerServerDlg::DisconnectClient(void) {
    socketManager_->TerminateSocketManager();
}

// ## 소켓 닫히면 출력하기 위한 콜백
LRESULT CRemoteExplorerServerDlg::OnShowMessage(WPARAM /*wparam*/, LPARAM lparam) {
    CString* msg = reinterpret_cast<CString*>(lparam);

    recvAllMsg_ = *msg + recvAllMsg_ ;
	valMsgInfo_.SetString(recvAllMsg_);

	UpdateData(FALSE);

    return 0;
}

void CRemoteExplorerServerDlg::OnBnClickedChkAutostart()
{
    // TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    UpdateData(true);

    if(valAutoStart_ == TRUE) {
        svcCtrlManager_->InstallSvc();
        svcCtrlManager_->StartSvc();

    } else if (valAutoStart_ == FALSE) {
        svcCtrlManager_->StopSvc();
        svcCtrlManager_->UninstallSvc();
    }
}
