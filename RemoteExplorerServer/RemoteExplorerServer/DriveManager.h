#ifndef DRIVEMANAGER_H
#define DRIVEMANAGER_H

#include "DriveInfo.h"
#include <vector>

class CDriveManager
{
private:

    std::vector<CDriveInfo*> driveInfoList_;
    std::vector<CString> drivePathList_;

public:
    CDriveManager(void);
    virtual ~CDriveManager(void);

    void CreateDriveInfoList(void);
    std::vector<CDriveInfo*> GetDriveInfoList(void);

private:
    void SearchDrive(void);
    bool IsWindowDrive(CString path);
    bool IsChildZero(CString path);
    CString GetDriveDisplayName(CString path);
    int GetDriveTypeIndex(CString path);
    bool GetIsWinDrive(CString path);
    bool GetIsChildZero(CString path);
};

#endif /*DRIVEMANAGER_H*/