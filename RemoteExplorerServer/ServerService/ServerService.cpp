// ServerService.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include <Windows.h>
#include <UserEnv.h>
#include <WtsApi32.h>
#include <TlHelp32.h>
#include <iostream>

TCHAR* serviceName = _T("RESService");
TCHAR* execPath = _T("C:\\Users\\frogsm\\Documents\\RemoteExplorer-frogsm\\RemoteExplorerServer\\Debug\\RemoteExplorerServer.exe");
SERVICE_STATUS_HANDLE svcStatusHandle;
DWORD nowState;

void RESSvcMain(DWORD argc, LPTSTR *argv);
void RESSvcHandler(DWORD fdwControl);
void RESSvcSetStatus(DWORD dwState, 
                     DWORD dwAccept 
                     = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE);
BOOL StartApp(LPWSTR lpwzPath);

int _tmain(int argc, _TCHAR* argv[])
{
    SERVICE_TABLE_ENTRY services[] = {
        {serviceName,
        (LPSERVICE_MAIN_FUNCTION)RESSvcMain},
        {NULL, NULL}
    };

    if(!StartServiceCtrlDispatcher(services)) {
        std::cout << _T("Service Start Fail") << std::endl;
        return -1;
    }

	return 0;
}

void RESSvcMain(DWORD argc, LPTSTR *argv) {
	svcStatusHandle =
		RegisterServiceCtrlHandlerEx(
			serviceName,
			(LPHANDLER_FUNCTION_EX)RESSvcHandler,
			(LPVOID)WTS_SESSION_LOGON
		);

    if(svcStatusHandle == FALSE) {
        return ;
    }

	if (!StartApp(execPath)) {
		return;
	}

    RESSvcSetStatus(SERVICE_START_PENDING, 0);
    RESSvcSetStatus(SERVICE_START);

	RESSvcSetStatus(SERVICE_RUNNING);
    
}

void RESSvcHandler(DWORD fdwControl) {
    HANDLE stopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

    if(fdwControl == nowState) {
        return ;
    }

    switch(fdwControl) {
    case SERVICE_CONTROL_PAUSE:
        RESSvcSetStatus(SERVICE_PAUSE_PENDING, 0);
        RESSvcSetStatus(SERVICE_PAUSED);
        break;
    case SERVICE_CONTROL_CONTINUE:
        RESSvcSetStatus(SERVICE_CONTINUE_PENDING, 0);
        RESSvcSetStatus(SERVICE_RUNNING);
        break;
    case SERVICE_CONTROL_STOP:
        RESSvcSetStatus(SERVICE_STOP_PENDING, 0);
        SetEvent(stopEvent);
        RESSvcSetStatus(SERVICE_STOPPED);
        break;
    case SERVICE_CONTROL_INTERROGATE:
    default:
        RESSvcSetStatus(nowState);
        break;
    }

}

void RESSvcSetStatus(DWORD dwState, DWORD dwAccept)
{
    SERVICE_STATUS sStatus;
    sStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    sStatus.dwCurrentState = dwState;
    sStatus.dwControlsAccepted = dwAccept;
    sStatus.dwWin32ExitCode = 0;
    sStatus.dwServiceSpecificExitCode = 0;
    sStatus.dwCheckPoint = 0;
    sStatus.dwWaitHint = 0;

    nowState = dwState;
    SetServiceStatus(svcStatusHandle, &sStatus);
}

BOOL StartApp(LPWSTR lpwzPath) {
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	BOOL bResult = FALSE;
	DWORD dwSessionId, winlogonPid;
	HANDLE hUserToken, hUserTokenDup, hPToken, hProcess;
	DWORD dwCreationFlags;

	// Log the client on to the local computer.  

	dwSessionId = WTSGetActiveConsoleSessionId();

	//////////////////////////////////////////  
	// Find the winlogon process  
	////////////////////////////////////////  

	PROCESSENTRY32 procEntry;

	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnap == INVALID_HANDLE_VALUE)
	{
		return 1;
	}

	procEntry.dwSize = sizeof(PROCESSENTRY32);

	if (!Process32First(hSnap, &procEntry))
	{
		return 1;
	}

	do
	{
		if (_tcscmp(procEntry.szExeFile, L"winlogon.exe") == 0)
		{
			// We found a winlogon process...make sure it's running in the console session  
			DWORD winlogonSessId = 0;
			if (ProcessIdToSessionId(procEntry.th32ProcessID, &winlogonSessId) && winlogonSessId == dwSessionId)
			{
				winlogonPid = procEntry.th32ProcessID;
				break;
			}
		}

	} while (Process32Next(hSnap, &procEntry));

	////////////////////////////////////////////////////////////////////////  

	WTSQueryUserToken(dwSessionId, &hUserToken);
	dwCreationFlags = NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE;
	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.lpDesktop = L"winsta0\\default";
	ZeroMemory(&pi, sizeof(pi));
	TOKEN_PRIVILEGES tp;
	LUID luid;
	hProcess = OpenProcess(MAXIMUM_ALLOWED, FALSE, winlogonPid);

	if (!::OpenProcessToken(hProcess, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY
		| TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY | TOKEN_ADJUST_SESSIONID
		| TOKEN_READ | TOKEN_WRITE, &hPToken))
	{
		int abcd = GetLastError();
		printf("Process token open Error: %u\n", GetLastError());
	}

	if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid))
	{
		printf("Lookup Privilege value Error: %u\n", GetLastError());
	}
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	DuplicateTokenEx(hPToken, MAXIMUM_ALLOWED, NULL, SecurityIdentification, TokenPrimary, &hUserTokenDup);
	int dup = GetLastError();

	//Adjust Token privilege  
	SetTokenInformation(hUserTokenDup, TokenSessionId, (void*)dwSessionId, sizeof(DWORD));

	if (!AdjustTokenPrivileges(hUserTokenDup, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), (PTOKEN_PRIVILEGES)NULL, NULL))
	{
		int abc = GetLastError();
		printf("Adjust Privilege value Error: %u\n", GetLastError());
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)
	{
		printf("Token does not have the provilege\n");
	}

	LPVOID pEnv = NULL;

	if (CreateEnvironmentBlock(&pEnv, hUserTokenDup, TRUE))
	{
		dwCreationFlags |= CREATE_UNICODE_ENVIRONMENT;
	}
	else
		pEnv = NULL;

	// Launch the process in the client's logon session.  

	bResult = CreateProcessAsUser(
		hUserTokenDup,            // client's access token  
		lpwzPath,              // file to execute  
		NULL,     // command line  
		NULL,              // pointer to process SECURITY_ATTRIBUTES  
		NULL,              // pointer to thread SECURITY_ATTRIBUTES  
		FALSE,             // handles are not inheritable  
		dwCreationFlags,  // creation flags  
		pEnv,              // pointer to new environment block   
		NULL,              // name of current directory   
		&si,               // pointer to STARTUPINFO structure  
		&pi                // receives information about new process  
	);
	// End impersonation of client.  

	//GetLastError Shud be 0  

	int iResultOfCreateProcessAsUser = GetLastError();

	//Perform All the Close Handles task  

	if (pi.hProcess != NULL) {
		/* 프로그램 실행되고 초기화 후 입력받을 수 있는 상태가 되면 다음코드로 진행*/
		DWORD dwWait = ::WaitForInputIdle(pi.hProcess, INFINITE);
		::CloseHandle(pi.hProcess);
	}

	SetForegroundWindow((HWND)pi.hProcess);

	CloseHandle(hProcess);
	CloseHandle(hUserToken);
	CloseHandle(hUserTokenDup);
	CloseHandle(hPToken);

	return bResult;
}