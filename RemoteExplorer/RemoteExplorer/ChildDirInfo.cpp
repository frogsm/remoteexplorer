#include "stdafx.h"
#include "ChildDirInfo.h"


CChildDirInfo::CChildDirInfo(CString name, bool isZero)
    : childName_(name), isDescendentZero_(isZero)
{
}

CChildDirInfo::~CChildDirInfo(void)
{
}

CString CChildDirInfo::GetChildName(void) {
    return childName_;
}

bool CChildDirInfo::GetIsDescendentZero(void) {
    return isDescendentZero_;
}