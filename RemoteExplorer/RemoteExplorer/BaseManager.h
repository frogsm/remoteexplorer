﻿#ifndef BASEMANAGER_H
#define BASEMANAGER_H

#include <string>

class AFX_EXT_CLASS CBaseManager
{
public:
    CBaseManager(void);
    virtual ~CBaseManager(void);

    virtual void Connect(void) = 0;
    virtual void Accept(void) = 0;
    virtual void Send(std::string msg) = 0;
    virtual void Receive(CString msg) = 0;
    virtual void Close(void) = 0;
};

#endif /*BASEMANAGER_H*/