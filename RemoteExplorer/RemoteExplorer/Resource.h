//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// RemoteExplorer.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_POPUP_EDIT                  119
#define ID_STATUSBAR_PANE1              120
#define ID_STATUSBAR_PANE2              121
#define IDS_STATUS_PANE1                122
#define IDS_STATUS_PANE2                123
#define IDS_TOOLBAR_STANDARD            124
#define IDS_TOOLBAR_CUSTOMIZE           125
#define ID_VIEW_CUSTOMIZE               126
#define ID_VIEW_ARRANGE                 127
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME_256               129
#define IDR_RemoteExplorerTYPE          130
#define IDR_THEME_MENU                  200
#define ID_SET_STYLE                    201
#define ID_VIEW_APPLOOK_WIN_2000        205
#define ID_VIEW_APPLOOK_OFF_XP          206
#define ID_VIEW_APPLOOK_WIN_XP          207
#define ID_VIEW_APPLOOK_OFF_2003        208
#define ID_VIEW_APPLOOK_VS_2005         209
#define ID_VIEW_APPLOOK_VS_2008         210
#define ID_VIEW_APPLOOK_OFF_2007_BLUE   215
#define ID_VIEW_APPLOOK_OFF_2007_BLACK  216
#define ID_VIEW_APPLOOK_OFF_2007_SILVER 217
#define ID_VIEW_APPLOOK_OFF_2007_AQUA   218
#define ID_VIEW_APPLOOK_WINDOWS_7       219
#define IDS_EDIT_MENU                   306
#define IDD_CONNECTSERVER               310
#define IDR_RE_MENU                     311
#define IDC_IPADDRESS1                  1000
#define IDC_EDIT_PORTNUM                1001
#define IDC_STATIC_SERVERNUM            1002
#define IDC_STATIC_PORTNUM              1003
#define ID_SOCKET_INIT                  2000
#define ID_SOCKET_CREATE                2001
#define ID_SOCKET_CONNECT               2002
#define ID_SOCKET_CLOSE                 2006
#define ID_DEFAULT_PORTNUM              2100
#define ID_DISK_LOCAL                   2200
#define ID_DISK_CD                      2201
#define ID_LIST_NAME                    2300
#define ID_LIST_UPDATEDATE              2301
#define ID_LIST_TYPE                    2302
#define ID_LIST_SIZE                    2303

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        312
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
