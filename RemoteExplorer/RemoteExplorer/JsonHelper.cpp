﻿#include "stdafx.h"
#include "JsonHelper.h"
#include "MainFrm.h"
#include "LeftView.h"
#include "RemoteExplorerView.h"
#include "TypeConvert.h"
#include "Protocol.h"

using namespace Json;

CJsonHelper::CJsonHelper(void)
{
    CMainFrame* pMainFrame = static_cast<CMainFrame*>(AfxGetMainWnd());
    doc_ = static_cast<CRemoteExplorerDoc*>(pMainFrame->GetActiveDocument());
}

CJsonHelper::~CJsonHelper(void)
{
}

std::string CJsonHelper::CreateJsonString(int command, CString path) {
	CTypeConvert typeConvert;

	FastWriter writer;
	std::string jsonMsg;
	Value jsonObject;

	switch (command) {
    case MESSAGE::DRIVEINFO:
	{
        jsonObject[JSON::COMMAND] = MESSAGE::DRIVEINFO;
	}
	break;

	case MESSAGE::CHILDDIRINFO:
	{
        jsonObject[JSON::COMMAND] = MESSAGE::CHILDDIRINFO;
        jsonObject[JSON::FULLPATH] = typeConvert.CStringToString(path);
	}
	break;

	case MESSAGE::DIRINFO:
	{
        jsonObject[JSON::COMMAND] = MESSAGE::DIRINFO;
        jsonObject[JSON::FULLPATH] = typeConvert.CStringToString(path);
	}
	break;

	default:
		break;
	}
	jsonMsg = writer.write(jsonObject);

    return jsonMsg;
}

void CJsonHelper::ParseJsonString(CString msg) {
	// ## MFC 변수
	CMainFrame* pMainFrame = static_cast<CMainFrame*>(AfxGetMainWnd());
	CRemoteExplorerDoc* doc = static_cast<CRemoteExplorerDoc*>(pMainFrame->GetActiveDocument());
	CLeftView *treeView = doc->GetTreeView();
	CRemoteExplorerView *listView = doc->GetListView();
	CTypeConvert typeConvert;

	// ## JSON 관련 변수.
	Reader reader;
	std::string jsonMsg;
	Value jsonObject;

	jsonMsg = typeConvert.CStringToString(msg);
	reader.parse(jsonMsg, jsonObject);

	int msgCommand = -1;
    msgCommand = jsonObject[JSON::COMMAND].asInt();

	switch (msgCommand)
	{
	case MESSAGE::DRIVEINFO:
	{
        Value driveList = jsonObject[JSON::DRIVELIST];
		std::vector<CDriveInfo*> driveInfoList;
		driveInfoList = GetDriveInfoList(driveList);
		treeView->SendMessage(UM_SHOW_DRIVE, 0, LPARAM(&driveInfoList));
	}
	break;
	case MESSAGE::CHILDDIRINFO:
	{
        Value childList = jsonObject[JSON::CHILDLIST];
        std::vector<CChildDirInfo*> childInfoList;
		childInfoList = GetChildDirInfoList(childList);
		treeView->SendMessage(UM_UPDATE_TREECTRL, 0, LPARAM(&childInfoList));
	}
	break;
    case MESSAGE::DIRINFO:
	{
        Value itemList = jsonObject[JSON::ITEMLIST];
        std::vector<CDirInfo*> dirInfoList;
		dirInfoList = GetDirInfoList(itemList);
		listView->SendMessage(UM_UPDATE_LISTCTRL, 0, LPARAM(&dirInfoList));
	}
	break;
	default:
		break;
	}
}

std::vector<CDriveInfo*> CJsonHelper::GetDriveInfoList(Value jsonObject) {

	std::vector<CDriveInfo*> driveInfoList;

	Value driveList = jsonObject;
	for (auto it = driveList.begin(); it != driveList.end(); ++it) {
        CString name((*it)[JSON::NAME].asCString());
        int type = (*it)[JSON::TYPE].asInt();
        bool isWin = (*it)[JSON::ISWIN].asBool();
        bool isChildZero = (*it)[JSON::ISCHILDZERO].asBool();

        CDriveInfo* driveInfo = new CDriveInfo(name, type, isWin, isChildZero);
        driveInfoList.push_back(driveInfo);
	}
	return driveInfoList;
}

std::vector<CChildDirInfo*> CJsonHelper::GetChildDirInfoList(Value jsonObject) {

	std::vector<CChildDirInfo*> childDirInfoList;

	Value childList = jsonObject;
	for (auto it = childList.begin(); it != childList.end(); ++it) {
        CString name((*it)[JSON::NAME].asCString());
        bool isDescendentZero = (*it)[JSON::ISDESCENDENTZERO].asBool();

		CChildDirInfo* childDirInfo = new CChildDirInfo(name, isDescendentZero);
		childDirInfoList.push_back(childDirInfo);
	}
	return childDirInfoList;
}

std::vector<CDirInfo*> CJsonHelper::GetDirInfoList(Value jsonObject) {

	std::vector<CDirInfo*> dirInfoList;

	Value itemInfoList = jsonObject;
	for (auto it = itemInfoList.begin(); it != itemInfoList.end(); ++it) {
        CString name((*it)[JSON::NAME].asCString());
        CString extension((*it)[JSON::EXTENSION].asCString());
        unsigned long long size = _strtoui64((*it)[JSON::SIZE].asString().c_str(), NULL, 10);
        unsigned long long updateDate = _strtoui64((*it)[JSON::UPDATEDATE].asString().c_str(), NULL, 10);


		CDirInfo* listFileInfo = new CDirInfo(name, extension, size, updateDate);
		dirInfoList.push_back(listFileInfo);
	}
	return dirInfoList;
}
