﻿#ifndef THREADMANAGER_H
#define THREADMANAGER_H

#include "SocketThread.h"
#include "ClientSocket.h"

class AFX_EXT_CLASS CThreadManager
{
public:
    CThreadManager(void);
    virtual ~CThreadManager(void);

    CSocketThread* CreateSocketThread(CClientSocket* socket);
    void DeleteSocketThread(CSocketThread* thread);
    void StopThread(CSocketThread* thread);

private:
    bool IsArrive(CSocketThread* thread);
    
};

#endif /*THREADMANAGER_H*/