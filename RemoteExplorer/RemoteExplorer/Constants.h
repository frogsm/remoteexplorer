﻿#ifndef CONSTANTS_H
#define CONSTANTS_H
// *********** 사용자 정의 윈도우 메세지 ***********

// ## 드라이브 정보 가져오는 윈도우메시지
#define UM_SHOW_DRIVE       WM_USER+1

// ## 트리 컨트롤 정보 업데이트 하는 윈도우메시지
#define UM_UPDATE_TREECTRL  WM_USER+2

// ## 리스트 뷰 정보 업데이트 하는 윈도우메시지
#define UM_UPDATE_LISTCTRL  WM_USER+3

// ## 리스트 컨트롤에서 트리 컨트롤에게 업데이트 요청하는 윈도우메시지
#define UM_UPDATE_LVDBCLICK WM_USER+4

#define UM_LC_UPDATE_ROOT   WM_USER+5

// ****************** 내부 상수 ******************

static const int MOVE_ROOT = 1000;
static const int MOVE_UP = 1001;
static const int MOVE_DOWN = 1002;

#endif /*CONSTANTS_H*/