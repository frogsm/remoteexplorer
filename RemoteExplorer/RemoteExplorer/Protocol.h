#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "stdafx.h"

class MESSAGE
{
public:
    static const int DRIVEINFO = 1;
    static const int CHILDDIRINFO = 2;
    static const int DIRINFO = 3;
};

class JSON {
public:
    static const int COMMAND = 1;
    static const int FULLPATH = 2;
    static const int DRIVELIST = 3;
    static const int NAME = 4;
    static const int TYPE = 5;
    static const int ISWIN = 6;
    static const int ISCHILDZERO = 7;

    static const int CHILDLIST = 8;
    static const int ISDESCENDENTZERO = 9;

    static const int ITEMLIST = 10;
    static const int EXTENSION = 11;
    static const int SIZE = 12;
    static const int UPDATEDATE = 13;
};   

#endif /*PROTOCOL_H*/