﻿#ifndef LISTENSOCKET_H
#define LISTENSOCKET_H

#include "afxsock.h"
#include "BaseManager.h"

class AFX_EXT_CLASS CListenSocket : public CAsyncSocket
{
private:
    CBaseManager* baseManager_;

    CListenSocket(void);

public:
    CListenSocket(CBaseManager* baseManager);
    virtual ~CListenSocket(void);

    virtual void OnAccept(int nErrorCode);
};

#endif /*LISTENSOCKET_H*/