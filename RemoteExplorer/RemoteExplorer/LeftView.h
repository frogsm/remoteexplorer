﻿
// LeftView.h : CLeftView 클래스의 인터페이스
//
#ifndef LEFTVIEW_H
#define LEFTVIEW_H

class CRemoteExplorerDoc;

class CLeftView : public CTreeView
{
protected: // serialization에서만 만들어집니다.
	CLeftView();
	DECLARE_DYNCREATE(CLeftView)

// 특성입니다.
public:
	CRemoteExplorerDoc* GetDocument();

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CLeftView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:

private :
	// ## 트리컨트롤의 루트.
	HTREEITEM hRoot_;
	// ## 리스트 컨트롤 이미지 파일.
	CImageList smallImageList_;

public:
	// ## 드라이브 정보 호출 하는 메세지
	afx_msg LRESULT ShowDriveInformation(WPARAM wparam, LPARAM lparam);

	// ## 한 항목에서 다른 항목으로 선택이 변경 되기 직전 (변경 되기 전, 호출)
	// ## 풀패스 저장로직 작성.
	afx_msg void OnTvnSelchanging(NMHDR *pNMHDR, LRESULT *pResult);

	// ## OnTvnSelchanging 메세지 처리기 작동 후 호출.
	// ## 리스트 뷰에 서버의 정보를 보여주기 위한 통신.
	afx_msg void OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult);

	// ## 사용자가 트리의 노드를 확장하는 중. ( 확장 되기 전, 호출)
	afx_msg void OnTvnItemexpanded(NMHDR *pNMHDR, LRESULT *pResult);

	// ## 전달받은 자식과 자손정보를 이용한 트리 컨트롤 업데이트.
	afx_msg LRESULT UpdateTreeCtrl(WPARAM wparam, LPARAM lparam);

	// ## 리스트 뷰에서의 폴더이동에 따른 트리 컨트롤 아이템 선택 변경.
	afx_msg LRESULT LVRequestProcessor(WPARAM wparam, LPARAM lparam);

	DECLARE_MESSAGE_MAP()

private:
    // ## 루트인지 확인
    bool IsRoot(CString selItem);

    // ## 드라이브인지 확인
    bool IsDrive(CString selItem, CString& treeCtrlFullPath);

    // ## 트리컨트롤 아이콘 초기화.
    void InitTreeCtrlImageList(CTreeCtrl& treeCtrl);

	// ## 가상드라이브 내컴퓨터 항목 추가
	void AddTreeCtrlRootInfo(CTreeCtrl& treeCtrl);

    // ## 드라이브 아이콘 인덱스 가져오기
    int GetDriveIconIndex(int driveType, bool isWin);

    void DeleteChildAllItems(HTREEITEM& selectedItem);

};

#ifndef _DEBUG  // LeftView.cpp의 디버그 버전
inline CRemoteExplorerDoc* CLeftView::GetDocument()
   { return reinterpret_cast<CRemoteExplorerDoc*>(m_pDocument); }
#endif

#endif /*LEFTVIEW_H*/