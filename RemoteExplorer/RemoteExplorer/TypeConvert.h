#ifndef TYPECONVERT_H
#define TYPECONVERT_H

#include <string>

class CTypeConvert
{
public:
    CTypeConvert(void);
    virtual ~CTypeConvert(void);
    CString IdToCString(int id);
    CString StringToCString(std::string str);
    std::string CStringToString(CString cstr);
    CString UllToCString(unsigned long long ull);
};

#endif /*CTYPECONVERT_H*/