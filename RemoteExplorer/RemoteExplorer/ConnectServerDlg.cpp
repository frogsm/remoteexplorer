﻿// ConnectServerDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "RemoteExplorer.h"
#include "ConnectServerDlg.h"
#include "afxdialogex.h"
#include "TypeConvert.h"




// CConnectServerDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CConnectServerDlg, CDialog)

CConnectServerDlg::CConnectServerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConnectServerDlg::IDD, pParent)
	, valIPAddress_(0)
	, valPortNum_(8080)
{

}

CConnectServerDlg::~CConnectServerDlg()
{
}

void CConnectServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IPADDRESS1, ctrlIPAddress_);
	DDX_Control(pDX, IDC_EDIT_PORTNUM, ctrlPortNum_);
	DDX_IPAddress(pDX, IDC_IPADDRESS1, valIPAddress_);
	DDX_Text(pDX, IDC_EDIT_PORTNUM, valPortNum_);
}


BEGIN_MESSAGE_MAP(CConnectServerDlg, CDialog)
	ON_WM_WINDOWPOSCHANGING()
END_MESSAGE_MAP()


// CConnectServerDlg 메시지 처리기입니다.


BOOL CConnectServerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
    CTypeConvert typeConvert;

    ctrlIPAddress_.SetAddress(127, 0, 0, 1);	// ## 자기 자신의 아이피 초기화.
    ctrlPortNum_.SetCueBanner(typeConvert.IdToCString(ID_DEFAULT_PORTNUM));	//	## 포트번호 공란일 경우 내용 표시.
	// ## End

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

CString CConnectServerDlg::GetIPString(void) {
    return convertIPStr_;
}

int CConnectServerDlg::GetPortNumber(void) {
    return valPortNum_;
}

void CConnectServerDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	// ## Start

	// DWORD로 받은 remoteIPAddress 정보 파싱 후 CString 형으로 변환.
    convertIPStr_ = _T("");
	unsigned long mask = 0x000000ffUL;
	int shift = 8;

	UpdateData(TRUE);

	while(valIPAddress_) {
		DWORD subIP = NULL;
		CString subIPStr ;

		subIP = valIPAddress_ & mask;
		valIPAddress_ >>= shift;

		subIPStr.Format(_T("%d"), subIP);

		if(valIPAddress_ != 0) {
			convertIPStr_ = _T(".") + subIPStr + convertIPStr_;
		}
		else {
			convertIPStr_ = subIPStr + convertIPStr_;
		}
	}

	// ## End
	CDialog::OnOK();
}


void CConnectServerDlg::OnCancel()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialog::OnCancel();
}

void CConnectServerDlg::OnWindowPosChanging(WINDOWPOS* /*lpwndpos*/)
{

}