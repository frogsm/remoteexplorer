﻿#ifndef DRIVEINFO_H
#define DRIVEINFO_H

#include <map>
#include <vector>

class CDriveInfo
{
private:
    CString driveName_;
    int driveType_;
    bool driveIsWin_;
    bool driveIsChildZero_;
    CDriveInfo(void);

public:
    CDriveInfo(CString name, int type, bool isWin, bool isChildOne);
    virtual ~CDriveInfo(void);

    CString GetName(void);
    int GetType(void);
    bool GetIsWin(void);
    bool GetIsChildZero(void);
};

#endif /*DRIVEINFO_H*/