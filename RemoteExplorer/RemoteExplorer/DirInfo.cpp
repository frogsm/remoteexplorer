#include "stdafx.h"
#include "DirInfo.h"


CDirInfo::CDirInfo(CString name, CString extension, unsigned long long size, unsigned long long updateDate)
    : itemName_(name), itemExtension_(extension)
    , itemSize_(size), itemUpdateDate_(updateDate)
{
}

CDirInfo::~CDirInfo(void)
{
}

CString CDirInfo::GetName(void) {
    return itemName_;
}

CString CDirInfo::GetExtension(void) {
    return itemExtension_;
}

unsigned long long CDirInfo::GetSize(void) {
    return itemSize_;
}

unsigned long long CDirInfo::GetUpdateDate(void) {
    return itemUpdateDate_;
}
