﻿
// RemoteExplorerView.h : CRemoteExplorerView 클래스의 인터페이스
//

#ifndef REMOTEEXPLORERVIEW_H
#define REMOTEEXPLORERVIEW_H

class CRemoteExplorerDoc;

class SORTITEM {
public:
	int ascending;
	int clickColumn;
};



class CRemoteExplorerView : public CListView
{
protected: // serialization에서만 만들어집니다.
	CRemoteExplorerView();
	DECLARE_DYNCREATE(CRemoteExplorerView)

// 특성입니다.
public:
	CRemoteExplorerDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CRemoteExplorerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	afx_msg void OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct);
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);

private:
	// ## 현재 선택된 리스트 컨트롤의 이름
	CString selectedItemName_;

	// ## 칼럼별 오름차순 정렬 여부
	bool ascending_[4];

	CImageList smallImageList_;
	CImageList largeImageList_;	

public:
	CString GetSelectedItemName(void);

	// ## 리스트 컨트롤을 업데이트.
	afx_msg LRESULT UpdateListCtrl(WPARAM wparam, LPARAM lparam);

    // ## 초기 루트정보 업데이트
    afx_msg LRESULT UpdateRootInfo(WPARAM wparam, LPARAM lparam);

	// ## 아이템 선택 변화 메세지 처리기.
	afx_msg void OnLvnItemchanging(NMHDR *pNMHDR, LRESULT *pResult);

	// ## 더블클릭 메세지 처리기.
	afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
	
	// ## 컬럼 헤더 클릭 메세지 처리기.
	afx_msg void OnHdnItemclick(NMHDR *pNMHDR, LRESULT *pResult);

	static int CALLBACK CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

private:
    void InitListCtrlImageList(CListCtrl& listCtrl);

    void UpdateFullPath(int& type);

    bool IsVirtual(CString& selName);
    bool IsRoot(CString& selName);
    bool IsDir(CString& extension);
    CString ConvertDate(CString& updateDate);
    CString ConvertSize(unsigned long long size);
    static int CompareSize(int a, int b);
    void SortListCtrlColumn(int column);
};

#ifndef _DEBUG  // RemoteExplorerView.cpp의 디버그 버전
inline CRemoteExplorerDoc* CRemoteExplorerView::GetDocument() const
   { return reinterpret_cast<CRemoteExplorerDoc*>(m_ument); }
#endif

#endif /*REMOTEEXPLORERVIEW_H*/