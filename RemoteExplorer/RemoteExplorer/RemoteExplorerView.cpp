﻿
// RemoteExplorerView.cpp : CRemoteExplorerView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "RemoteExplorer.h"
#endif

#include "RemoteExplorerDoc.h"
#include "RemoteExplorerView.h"
#include "ConnectServerDlg.h"
#include "LeftView.h"
#include "json\json.h"
#include "TypeConvert.h"
#include "DirInfo.h"

using namespace Json;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CRemoteExplorerView

IMPLEMENT_DYNCREATE(CRemoteExplorerView, CListView)

BEGIN_MESSAGE_MAP(CRemoteExplorerView, CListView)
	ON_WM_STYLECHANGED()
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
    ON_MESSAGE(UM_LC_UPDATE_ROOT, UpdateRootInfo)
	ON_MESSAGE(UM_UPDATE_LISTCTRL, UpdateListCtrl)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CRemoteExplorerView::OnNMDblclk)
	ON_NOTIFY_REFLECT(LVN_ITEMCHANGING, &CRemoteExplorerView::OnLvnItemchanging)
	ON_NOTIFY(HDN_ITEMCLICKA, 0, &CRemoteExplorerView::OnHdnItemclick)
	ON_NOTIFY(HDN_ITEMCLICKW, 0, &CRemoteExplorerView::OnHdnItemclick)
END_MESSAGE_MAP()

// CRemoteExplorerView 생성/소멸

CRemoteExplorerView::CRemoteExplorerView() : selectedItemName_(" ")
{
	// TODO: 여기에 생성 코드를 추가합니다.
	for(int i = 0 ; i<4 ; i++)
		ascending_[i] = true;
}

CRemoteExplorerView::~CRemoteExplorerView()
{
}

BOOL CRemoteExplorerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CListView::PreCreateWindow(cs);
}

void CRemoteExplorerView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();

	// TODO: GetListCtrl()을 호출하여 해당 list 컨트롤을 직접 액세스함으로써
	//  ListView를 항목으로 채울 수 있습니다.
    CTypeConvert typeConvert;
	CListCtrl& listCtrl = GetListCtrl();

	// ## 리스트 컨트롤 스타일 초기화.
	listCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	// ## 리스트 컨트롤의 이미지리스트 초기화.
    InitListCtrlImageList(listCtrl);

	// ## 리스트 컨트롤의 컬럼정보 넣기.
	CRect rect;
	listCtrl.GetClientRect(&rect);
    listCtrl.InsertColumn(0, typeConvert.IdToCString(ID_LIST_NAME), LVCFMT_LEFT, 300);
    listCtrl.InsertColumn(1, typeConvert.IdToCString(ID_LIST_UPDATEDATE), LVCFMT_LEFT, 160);
    listCtrl.InsertColumn(2, typeConvert.IdToCString(ID_LIST_TYPE), LVCFMT_LEFT, 120);
    listCtrl.InsertColumn(3, typeConvert.IdToCString(ID_LIST_SIZE), LVCFMT_LEFT, 120);
}

void CRemoteExplorerView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CRemoteExplorerView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}



// CRemoteExplorerView 진단

#ifdef _DEBUG
void CRemoteExplorerView::AssertValid() const
{
	CListView::AssertValid();
}

void CRemoteExplorerView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CRemoteExplorerDoc* CRemoteExplorerView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRemoteExplorerDoc)));
	return (CRemoteExplorerDoc*)m_pDocument;
}
#endif //_DEBUG


CString CRemoteExplorerView::GetSelectedItemName(void) {
	return selectedItemName_;
}

// CRemoteExplorerView 메시지 처리기
void CRemoteExplorerView::OnStyleChanged(int nStyleType, LPSTYLESTRUCT lpStyleStruct)
{
	//TODO: 사용자가 창의 뷰 스타일을 변경했을 때 반응하는 코드를 추가합니다.	
	CListView::OnStyleChanged(nStyleType,lpStyleStruct);	
}

// ## 현재 리스트뷰의 모든 아이템을 삭제하고 서버로부터 받아온 아이템을 삽입하여 갱신.
LRESULT CRemoteExplorerView::UpdateListCtrl(WPARAM /*wparam*/, LPARAM lparam) {
	// ## 파라미터로 가져온 메세지를 CString으로 변환.
	CRemoteExplorerDoc* doc = GetDocument();
    std::vector<CDirInfo*>* itemList = (std::vector<CDirInfo*>*)lparam;
    CTypeConvert typeConvert;

	CListCtrl& listCtrl = GetListCtrl();

	// ## 현재 리스트뷰의 모든 아이템 삭제.
	listCtrl.DeleteAllItems();

	// ## 리스트 컨트롤에 ".."가상폴더 삽입.
	listCtrl.InsertItem(0, _T(".."), doc->GetIconIndex(_T(" ")));

	int itemSize = itemList->size();
	for (int itemIndex = 0; itemIndex < itemSize; itemIndex++) {
		CDirInfo* item = itemList->at(itemIndex);
		CString name = item->GetName();
		CString extension = item->GetExtension();

        CString size = typeConvert.UllToCString(item->GetSize());
        if(IsDir(extension)) {
            size = _T(" ");
        } else if(!IsDir(extension)) {
            size = ConvertSize(item->GetSize());
        }

        CString updateDate = typeConvert.UllToCString(item->GetUpdateDate());
        CString type = doc->GetTypeStr(name, extension);

		listCtrl.InsertItem(itemIndex+1, name, doc->GetIconIndex(extension));
        listCtrl.SetItemText(itemIndex+1, 1, ConvertDate(updateDate));
		listCtrl.SetItemText(itemIndex+1, 2, type);
		listCtrl.SetItemText(itemIndex+1, 3, size);

        if(item != nullptr) {
            delete item;
        }
	}
	return 0;
}

LRESULT CRemoteExplorerView::UpdateRootInfo(WPARAM /*wparam*/, LPARAM /*lparam*/) {

    CListCtrl& listCtrl = GetListCtrl();

    // ## 현재 리스트뷰의 모든 아이템 삭제.
	listCtrl.DeleteAllItems();

    // ## 트리 컨트롤의 루트노드(내 컴퓨터)가 선택된 경우 
    CRemoteExplorerDoc* doc = GetDocument();
    vector<pair<CString, int>> &driveList = doc->GetDriveList();
    int itemIndex = 0;
    for(auto it = driveList.begin() ; it != driveList.end() ; ++it) {
        listCtrl.InsertItem(itemIndex++, (*it).first, (*it).second);
    }
    return 0;
}

// ## 아이템 항목 변화시 현재 선택된 이름을 이용하여 현재 path 변경
void CRemoteExplorerView::OnLvnItemchanging(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CListCtrl& listCtrl = GetListCtrl();

	if(pNMLV->uNewState & LVIS_SELECTED) {
		selectedItemName_  = listCtrl.GetItemText(pNMLV->iItem, 0);	// ## 이름 얻기.
	} else {
		selectedItemName_ = _T(" ");
	}
	*pResult = 0;
}

// ## 트리 컨트롤에게 루트 변경의 요청을 신청한다.
void CRemoteExplorerView::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

    int clkIndex = pNMItemActivate->iItem;

	// ## 공백 더블클릭 했을 경우 예외처리.
	if(clkIndex == -1) {
		return ;
	}

	// ## 필요 변수 초기화.
	CRemoteExplorerDoc* doc = GetDocument();
    CLeftView* treeView = doc->GetTreeView();
	CString copyTreeCtrlFullPath = doc->GetListCtrlFullPath();
	CString& listCtrlFullPath = doc->GetListCtrlFullPath();

	listCtrlFullPath = copyTreeCtrlFullPath;

	int requestType = -1;
    UpdateFullPath(requestType);

	// ## 트리 컨트롤에게 메세지 전송.
    treeView->SendMessage(UM_UPDATE_LVDBCLICK, 0, LPARAM(requestType));
	selectedItemName_ = _T(" ");

	*pResult = 0;
}

void CRemoteExplorerView::OnHdnItemclick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int clickColumn = phdr->iItem;

	ascending_[clickColumn] = !ascending_[clickColumn];
    SortListCtrlColumn(clickColumn);

	*pResult = 0;
}

int CALLBACK CRemoteExplorerView::CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParam)
{
    CString* pStr1 = reinterpret_cast<CString*>(lParam1); 
    CString* pStr2 = reinterpret_cast<CString*>(lParam2);
    SORTITEM* sortItem  = reinterpret_cast<SORTITEM*>(lParam);

	int asc = sortItem->ascending;
	int column = sortItem->clickColumn;

	// ## 선택한 칼럼이 크기일 경우 비교.
	if(column == 3) {
		int pNum1 = _ttoi(pStr1->Left(pStr1->GetLength() - 2));
        if(_tcscmp(*pStr1, _T(" ")) == 0) {
            pNum1 = -10;
        }
		int pNum2 = _ttoi(pStr2->Left(pStr2->GetLength() - 2));
        if(_tcscmp(*pStr2, _T(" ")) == 0) {
            pNum2 = -10;
        }
		
        return asc == 1 ? CompareSize(pNum1, pNum2) : -CompareSize(pNum1, pNum2);
	}

    return asc == 1 ? _tcscmp(*pStr1, *pStr2) : -_tcscmp(*pStr1, *pStr2);
}

void CRemoteExplorerView::InitListCtrlImageList(CListCtrl& listCtrl) {
    LPITEMIDLIST item = NULL;
	SHGetSpecialFolderLocation(NULL, CSIDL_DESKTOP, &item);

	SHFILEINFO sfiSmall, sfiLarge;
	ZeroMemory(&sfiSmall, sizeof(SHFILEINFO));
	ZeroMemory(&sfiLarge, sizeof(SHFILEINFO));

    smallImageList_.Attach((HIMAGELIST)SHGetFileInfo(reinterpret_cast<LPCWSTR>(item), 0, &sfiSmall, sizeof(sfiSmall), SHGFI_SMALLICON | SHGFI_SYSICONINDEX));
	listCtrl.SetImageList(&smallImageList_, LVSIL_SMALL);
    largeImageList_.Attach((HIMAGELIST)SHGetFileInfo(reinterpret_cast<LPCWSTR>(item), 0, &sfiLarge, sizeof(sfiLarge), SHGFI_LARGEICON | SHGFI_SYSICONINDEX));
	listCtrl.SetImageList(&largeImageList_, TVSIL_NORMAL);

    DestroyIcon(static_cast<HICON>(sfiSmall.hIcon));
	DestroyIcon(static_cast<HICON>(sfiLarge.hIcon));

	smallImageList_.Detach();
	largeImageList_.Detach();
}

void CRemoteExplorerView::UpdateFullPath(int& type) {

    CString& listCtrlFullPath = GetDocument()->GetListCtrlFullPath();
    CString parentPath = listCtrlFullPath;

    if(IsVirtual(selectedItemName_)) {
        int splitPos = -1;

        parentPath.Delete(parentPath.GetLength()-1, 2);
        splitPos = parentPath.ReverseFind('\\');
        parentPath = parentPath.Left(splitPos+1);
    }

    if(IsRoot(parentPath)) {
        listCtrlFullPath = _T("\\");
        type = MOVE_ROOT;
        return ;
    }

    if(!IsRoot(parentPath)) {
        listCtrlFullPath = parentPath;
        type= MOVE_UP;
    }

    if(!IsVirtual(selectedItemName_)) {
        listCtrlFullPath = listCtrlFullPath + selectedItemName_ + _T("\\");
        type = MOVE_DOWN;
        return ;
    }

    return ;
}

bool CRemoteExplorerView::IsVirtual(CString& selName) {
    return _tcscmp(selName, _T("..")) == 0 ? true : false;
}

bool CRemoteExplorerView::IsRoot(CString& selName) {
    return _tcscmp(selName, _T("")) == 0 ? true : false;
}

bool CRemoteExplorerView::IsDir(CString& extension) {
    return _tcscmp(extension, _T(" ")) == 0 ? true : false;
}

CString CRemoteExplorerView::ConvertDate(CString& updateDate) {
    CString retCSt;
    int mid = updateDate.GetLength() / 2;
    CString year = updateDate.Left(4);
    CString month = updateDate.Mid(mid-2, 2);
    CString day = updateDate.Mid(mid, 2);
    CString hour = updateDate.Mid(mid+2, 2);
    CString minute = updateDate.Right(2);
    
    int hourInt = _ttoi(hour);
    if(hourInt < 12) {
        hour = _T("오전");
    }
    else if(hourInt >= 12) {
        hour = _T("오후");
    }
    hourInt %= 12;
    retCSt.Format(_T("%s-%s-%s %s %02d:%s"), year, month, day, hour, hourInt, minute);

    return retCSt;
}

CString CRemoteExplorerView::ConvertSize(unsigned long long size) {
    TCHAR buffer[100];
    CString retCStr;

    long double byteToKB = (long double)size / static_cast<long double>(1024);
    long double round = floor(byteToKB);
    if( (byteToKB - round) < 0.5 )  {
        size = (unsigned long long) round;
    }
    else {
        size = (unsigned long long) round + 1;
    }
     _ui64tot_s(size, buffer, 100, 10);
     retCStr.Format(_T("%sKB"), buffer);

    return retCStr;
}

int CRemoteExplorerView::CompareSize(int a, int b) {
    if (a > b) {
        return 1;
    } else if (a < b) {
        return -1;
    }
    return 0;
}

void CRemoteExplorerView::SortListCtrlColumn(int column) {
    CListCtrl& listCtrl = GetListCtrl();
    CRemoteExplorerDoc* doc = GetDocument();
    int nItem = listCtrl.GetItemCount();

    listCtrl.DeleteItem(0);

	CString **itemName = new CString*[nItem];
	for(int i=0 ; i<nItem ; i++) {
		itemName[i] = new CString(listCtrl.GetItemText(i, column));
		listCtrl.SetItemData(i,  (LPARAM)itemName[i]);
	}

    SORTITEM sortItem;
    sortItem.ascending = ascending_[column];
	sortItem.clickColumn = column;

    listCtrl.SortItems(CompareFunc, (LPARAM)&sortItem);
    listCtrl.InsertItem(0, _T(".."), doc->GetIconIndex(_T(" ")));

    for (int i=0; i<nItem; i++) {
        if(itemName[i] != nullptr) {
            delete itemName[i];
        }
    }
    if(itemName != nullptr) {
        delete []itemName;
    }
}
