﻿
// RemoteExplorerDoc.cpp : CRemoteExplorerDoc 클래스의 구현

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "RemoteExplorer.h"
#endif

#include "MainFrm.h"
#include "RemoteExplorerDoc.h"
#include "TypeConvert.h"
#include "ClientSocket.h"
#include <propkey.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CRemoteExplorerDoc

IMPLEMENT_DYNCREATE(CRemoteExplorerDoc, CDocument)

BEGIN_MESSAGE_MAP(CRemoteExplorerDoc, CDocument)
END_MESSAGE_MAP()


// CRemoteExplorerDoc 생성/소멸

CRemoteExplorerDoc::CRemoteExplorerDoc()
{
	// TODO: 여기에 일회성 생성 코드를 추가합니다.
	SetImageIndex();
	driveList_ = new vector<pair<CString, int>>();
	treeCtrlFullPath_ = new CString();
	listCtrlFullPath_ = new CString();
}

CRemoteExplorerDoc::~CRemoteExplorerDoc()
{
    if(socketManager_ != nullptr) {
        delete socketManager_;
    }
    if(listCtrlFullPath_ != nullptr) {
        delete listCtrlFullPath_;
    }
    if(treeCtrlFullPath_ != nullptr) {
        delete treeCtrlFullPath_;
    }
    if(driveList_ != nullptr) {
        delete driveList_;
    }
}

BOOL CRemoteExplorerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 여기에 재초기화 코드를 추가합니다.
	// SDI 문서는 이 문서를 다시 사용합니다.
    
    CMainFrame* pMainFrame = static_cast<CMainFrame*>(AfxGetMainWnd());

    CString ip = pMainFrame->clientIPAddr;
    int port = pMainFrame->clientPortNum;

    socketManager_ = new CSocketManager();
    socketManager_->InitClientSocket(ip, port);

	return TRUE;
}

CSocketManager* CRemoteExplorerDoc::GetSocketManager(void) const {
    return socketManager_;
}

CString& CRemoteExplorerDoc::GetTreeCtrlFullPath(void) const {
	return *treeCtrlFullPath_;
}

CString& CRemoteExplorerDoc::GetListCtrlFullPath(void) const {
	return *treeCtrlFullPath_;
}

vector<pair<CString, int>>& CRemoteExplorerDoc::GetDriveList(void) const {
	return *driveList_;
}


int CRemoteExplorerDoc::GetFolderImageIndex(int size) {

	if(size == 0) {			// smallIcon
		return folderImgSmall_;
	} else if(size == 1) {	// largeIcon
		return folderImgLarge_;
	}

	return 0;
}

int CRemoteExplorerDoc::GetWinLocalDriveImageIndex(int size) {

	if(size == 0) {			// smallIcon
		return winLocalDriveImgSmall_;
	} else if(size == 1) {	// largeIcon
		return winLocalDriveImgLarge_;
	}

	return 0;
}

int CRemoteExplorerDoc::GetLocalDriveImageIndex(int size) {

	if(size == 0) {			// smallIcon
		return localDriveImgSmall_;
	} else if(size == 1) {	// largeIcon
		return localDriveImgLarge_;
	}

	return 0;
}

int CRemoteExplorerDoc::GetCdDriveImageIndex(int size) {

	if(size == 0) {			// smallIcon
		return cdDriveImgSmall_;
	} else if(size == 1) {	// largeIcon
		return cdDriveImgLarge_;
	}

	return 0;
}


CLeftView* CRemoteExplorerDoc::GetTreeView(void) {
	POSITION pos = GetFirstViewPosition();
	CLeftView *pView = (CLeftView*)GetNextView(pos);

	return pView;
}

CRemoteExplorerView* CRemoteExplorerDoc::GetListView(void) {
	POSITION pos = GetFirstViewPosition();
    CRemoteExplorerView *pView = nullptr;
	while(pos != NULL) {
		pView = (CRemoteExplorerView*)GetNextView(pos);
	}

	return pView;
}


CString CRemoteExplorerDoc::GetTypeStr(CString name, CString extension) {

	CString path, displayType;

    if(_tcscmp(extension, _T(" ")) == 0) {
		// ## 디렉토리 인경우
		path = *treeCtrlFullPath_ + name;
	} else {
		// ## 일반 실행파일인 경우
		path = *treeCtrlFullPath_ + name + _T(".") + extension;
	}

	SHFILEINFO sfi;
	ZeroMemory(&sfi, sizeof(SHFILEINFO));
	SHGetFileInfo(path, 0, &sfi, sizeof(sfi), SHGFI_TYPENAME | SHGFI_EXETYPE);

	displayType.Format(_T("%s"), sfi.szTypeName);

	return displayType;
}

int CRemoteExplorerDoc::GetIconIndex(CString extension) {

	int retIndex ;

	// ## 폴더 예외처리.
	if(extension == _T(" ")) {
        return folderImgSmall_;
    }

	CString paramExtension = _T(".") + extension;

	SHFILEINFO sfi;
	ZeroMemory(&sfi, sizeof(SHFILEINFO));
	SHGetFileInfo(paramExtension, 0, &sfi, sizeof(sfi), SHGFI_USEFILEATTRIBUTES | SHGFI_ICONLOCATION | SHGFI_ICON | SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_LARGEICON);
	retIndex = sfi.iIcon;

    DestroyIcon(static_cast<HICON>(sfi.hIcon));

	return retIndex;
}


void CRemoteExplorerDoc::SetImageIndex(void) {

    SetIconIndex(folderImgSmall_, GetWinDrive(), SHGFI_SMALLICON);
    SetIconIndex(folderImgLarge_, GetWinDrive(), SHGFI_LARGEICON);
    
    CString driveName = _T("?:\\");
    char drivePos = static_cast<char>(0);
    DWORD buffer = GetLogicalDrives();

    while(buffer) {
        if(buffer & 1) {
            driveName.SetAt(0, 'A'+drivePos);
             
            CString driveType = GetTypeDrive(driveName);
            if(IsWinDrive(driveName)) {
                SetIconIndex(winLocalDriveImgSmall_, driveName, SHGFI_SMALLICON);
                SetIconIndex(winLocalDriveImgLarge_, driveName, SHGFI_LARGEICON);
            } 
            else if(IsLocalDrive(driveType)) {
                SetIconIndex(localDriveImgSmall_, driveName, SHGFI_SMALLICON);
                SetIconIndex(localDriveImgLarge_, driveName, SHGFI_LARGEICON);
            } 
            else if (IsCDDrive(driveType)) {
                SetIconIndex(cdDriveImgSmall_, driveName, SHGFI_SMALLICON);
                SetIconIndex(cdDriveImgLarge_, driveName, SHGFI_LARGEICON);
            }

        }
        buffer = buffer >> 1;
        drivePos++;
    }
}

void CRemoteExplorerDoc::SetIconIndex(int& icon, CString path, UINT addFlag) {
    SHFILEINFO sfi;
    UINT defalutFlag = SHGFI_SYSICONINDEX | SHGFI_TYPENAME;
    UINT flag = defalutFlag | addFlag;

	ZeroMemory(&sfi, sizeof(SHFILEINFO));

    SHGetFileInfo(path, 0, &sfi, sizeof(sfi), flag);
    icon = sfi.iIcon;

    DestroyIcon((HICON)sfi.iIcon);
}

CString CRemoteExplorerDoc::GetWinDrive(void) {
    CString winProgramPath;

    LPITEMIDLIST item = NULL;
    SHGetSpecialFolderLocation(NULL, CSIDL_WINDOWS, &item);
    SHGetPathFromIDList(item, const_cast<LPWSTR>(static_cast<LPCTSTR>(winProgramPath)));

    return winProgramPath;
}

CString CRemoteExplorerDoc::GetTypeDrive(CString path) {
    SHFILEINFO sfi;
	ZeroMemory(&sfi, sizeof(SHFILEINFO));

    SHGetFileInfo(path, 0, &sfi, sizeof(sfi), SHGFI_TYPENAME);

    return sfi.szTypeName;
}


bool CRemoteExplorerDoc::IsWinDrive(CString driveName) {
    CString winProgramPath = GetWinDrive();

    return driveName[0] == winProgramPath[0] ? true : false;
}

bool CRemoteExplorerDoc::IsLocalDrive(CString driveName) {
    CTypeConvert typeConvert;
    CString localDisk = typeConvert.IdToCString(ID_DISK_LOCAL);
    
    return _tcscmp(driveName, localDisk) == 0 ? true : false;
}

bool CRemoteExplorerDoc::IsCDDrive(CString driveName) {
    CTypeConvert typeConvert;
    CString cdDisk = typeConvert.IdToCString(ID_DISK_CD);

    return _tcscmp(driveName, cdDisk) == 0 ? true : false;
}


// CRemoteExplorerDoc serialization

void CRemoteExplorerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 여기에 저장 코드를 추가합니다.
	}
	else
	{
		// TODO: 여기에 로딩 코드를 추가합니다.
	}
}

#ifdef SHARED_HANDLERS

// 축소판 그림을 지원합니다.
void CRemoteExplorerDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// 문서의 데이터를 그리려면 이 코드를 수정하십시오.
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 검색 처리기를 지원합니다.
void CRemoteExplorerDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// 문서의 데이터에서 검색 콘텐츠를 설정합니다.
	// 콘텐츠 부분은 ";"로 구분되어야 합니다.

	// 예: strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CRemoteExplorerDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CRemoteExplorerDoc 진단

#ifdef _DEBUG
void CRemoteExplorerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRemoteExplorerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CRemoteExplorerDoc 명령