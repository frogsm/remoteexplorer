﻿#ifndef SOCKETMANAGER_H
#define SOCKETMANAGER_H

#include "BaseManager.h"
#include "ClientSocket.h"

class CSocketManager : public CBaseManager
{
private:
    CClientSocket* clientSocket_;

public:
    CSocketManager(void);
    virtual ~CSocketManager(void);

    void InitClientSocket(CString ip, int port);
    void TerminateClientSocket(void);

    virtual void Connect(void);
    virtual void Accept(void);
    virtual void Send(std::string msg);
    virtual void Receive(CString msg);
    virtual void Close(void);
};

#endif /*SOCKETMANAGER_H*/