#ifndef DIRINFO_H
#define DIRINFO_H

class CDirInfo
{
private:
    CString itemName_;
    CString itemExtension_;
    unsigned long long itemSize_;
    unsigned long long itemUpdateDate_;

    CDirInfo(void);

public:
    CDirInfo(CString name, CString extension, unsigned long long size, unsigned long long updateDate);
    virtual ~CDirInfo(void);
    CString GetName(void);
    CString GetExtension(void);
    unsigned long long GetSize(void);
    unsigned long long GetUpdateDate(void);
};

#endif /*DIRINFO_H*/
