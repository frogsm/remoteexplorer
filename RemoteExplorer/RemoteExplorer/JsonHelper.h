﻿#ifndef JSONHELPER_H
#define JSONHELPER_H

#include "json\json.h"
#include "MainFrm.h"
#include "RemoteExplorerDoc.h"
#include "DriveInfo.h"
#include "ChildDirInfo.h"
#include "DirInfo.h"

#include <vector>

class CJsonHelper
{
private:
    CRemoteExplorerDoc* doc_;

public:
    CJsonHelper(void);
    virtual ~CJsonHelper(void);
    std::string CreateJsonString(int command, CString path = CString());
    void ParseJsonString(CString msg);

private:
    std::vector<CDriveInfo*> GetDriveInfoList(Json::Value jsonObject);
    std::vector<CChildDirInfo*> GetChildDirInfoList(Json::Value jsonObject);
    std::vector<CDirInfo*> GetDirInfoList(Json::Value jsonObject);
};

#endif /*JSONHELPER_H*/