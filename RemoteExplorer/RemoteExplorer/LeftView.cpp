﻿
// LeftView.cpp : CLeftView 클래스의 구현
//

#include "stdafx.h"
#include "RemoteExplorer.h"
#include "RemoteExplorerDoc.h"
#include "LeftView.h"
#include "RemoteExplorerView.h"
#include "json\json.h"
#include "DriveInfo.h"
#include "ChildDirInfo.h"
#include "JsonHelper.h"
#include "Protocol.h"

using namespace Json;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLeftView

IMPLEMENT_DYNCREATE(CLeftView, CTreeView)

BEGIN_MESSAGE_MAP(CLeftView, CTreeView)
	ON_MESSAGE(UM_SHOW_DRIVE, ShowDriveInformation)
	ON_MESSAGE(UM_UPDATE_TREECTRL, UpdateTreeCtrl)
	ON_MESSAGE(UM_UPDATE_LVDBCLICK, LVRequestProcessor)
	ON_NOTIFY_REFLECT(TVN_SELCHANGING, &CLeftView::OnTvnSelchanging)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDED, &CLeftView::OnTvnItemexpanded)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, &CLeftView::OnTvnSelchanged)
END_MESSAGE_MAP()


// CLeftView 생성/소멸

CLeftView::CLeftView()
{
	// TODO: 여기에 생성 코드를 추가합니다.
}

CLeftView::~CLeftView()
{
}

BOOL CLeftView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서 Window 클래스 또는 스타일을 수정합니다.

	return CTreeView::PreCreateWindow(cs);
}

void CLeftView::OnInitialUpdate()
{
	CTreeView::OnInitialUpdate();

	// TODO: TreeView를 항목으로 채우려면
	//  GetTreeCtrl()을 호출하여 해당 tree 컨트롤을 직접 액세스하십시오.

	CTreeCtrl& treeCtrl = GetTreeCtrl();
    CJsonHelper jsonHelper;
    std::string msg;
    
    InitTreeCtrlImageList(treeCtrl);
    AddTreeCtrlRootInfo(treeCtrl);

    // ## 메세지 생성
    msg = jsonHelper.CreateJsonString(MESSAGE::DRIVEINFO);
    GetDocument()->GetSocketManager()->Send(msg);

    // ## 트리컨트롤 스타일 수정.
    treeCtrl.ModifyStyle(NULL, TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT | TVS_SHOWSELALWAYS);
    smallImageList_.Detach();
}


// CLeftView 진단

#ifdef _DEBUG
void CLeftView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CLeftView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}

CRemoteExplorerDoc* CLeftView::GetDocument() // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRemoteExplorerDoc)));
	return (CRemoteExplorerDoc*)m_pDocument;
}
#endif //_DEBUG


// CLeftView 메시지 처리기

// ## jsonObject의 dirveList의 수 만큼, 드라이브를 하나씩 빼와서 hRoot에 삽입.
LRESULT CLeftView::ShowDriveInformation(WPARAM /*wparam*/, LPARAM lparam) {
    CTreeCtrl& treeCtrl = GetTreeCtrl();
    CRemoteExplorerDoc* doc = GetDocument();
    CRemoteExplorerView *listView = doc->GetListView();
	vector<pair<CString, int>>& storeDriveList = doc->GetDriveList();
    std::vector<CDriveInfo*>* driveList = (std::vector<CDriveInfo*>*)lparam;

    HTREEITEM childItem;
    
    int driveSize = driveList->size();
    for(int driveIndex = 0; driveIndex < driveSize ; driveIndex++) {
        CDriveInfo* drive = driveList->at(driveIndex);
        CString name = drive->GetName();
        int type = drive->GetType();
        bool isWin = drive->GetIsWin();
        bool isChildZero = drive->GetIsChildZero();
        int icon = GetDriveIconIndex(type, isWin);

		storeDriveList.push_back(make_pair(name, icon));
       	childItem = treeCtrl.InsertItem(name, icon, icon, hRoot_);

        if(!isChildZero) {
            int dirIcon = doc->GetFolderImageIndex(0);
            treeCtrl.InsertItem(_T(""), dirIcon, dirIcon, childItem);
        }
        if(drive != nullptr) {
            delete drive;
        }
    }

	// ## 드라이브 정보에 대한 리스트뷰 출력.
    listView->SendMessage(UM_LC_UPDATE_ROOT, 0, 0);

	// ## 루트 아이템 확장.
	treeCtrl.Expand(GetTreeCtrl().GetRootItem(), TVE_EXPAND);

	return 0;
}

// ## 현재 선택된 폴더이름을 이용하여 fullPath 생성
void CLeftView::OnTvnSelchanging(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
    //pNMTreeView = nullptr;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CTreeCtrl& treeCtrl = GetTreeCtrl();

	CRemoteExplorerDoc* doc = GetDocument();
	CString& treeCtrlFullPath = doc->GetTreeCtrlFullPath();

	treeCtrlFullPath = _T("\\");

	// ## 현재 선택된 아이템.
	
    //if(pNMTreeView == nullptr) {
    //    return ;
    //}
    HTREEITEM item = pNMTreeView->itemNew.hItem;
	CString dirName = treeCtrl.GetItemText(item);

    if(IsDrive(dirName, treeCtrlFullPath) || IsRoot(dirName)) {
        return ;
    }

	treeCtrlFullPath = dirName + treeCtrlFullPath;

    // ## 부모 타고 올라가면서 FullPath 생성.
    item = treeCtrl.GetNextItem(item,TVGN_PARENT);
    dirName = treeCtrl.GetItemText(item);
    while(!IsDrive(dirName, treeCtrlFullPath) && !IsRoot(dirName)) {
        treeCtrlFullPath = dirName + _T("\\") + treeCtrlFullPath;
        item = treeCtrl.GetNextItem(item,TVGN_PARENT);
		dirName = treeCtrl.GetItemText(item);
    }

	*pResult = 0;
}

// ## 서버에게 fullPath를 보내서, 경로에 해당하는 [모든 디렉터리] & [모든 파일] 정보를 얻어온다.
void CLeftView::OnTvnSelchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
    pNMTreeView = nullptr;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CRemoteExplorerDoc* doc = GetDocument();
	CString& treeCtrlFullPath = doc->GetTreeCtrlFullPath();
    CJsonHelper jsonHelper;
    std::string msg;
	
	// ## 루트노드의 path는 서버전송 X
    if(IsRoot(treeCtrlFullPath)) {
		CRemoteExplorerView *listView = doc->GetListView();
        listView->SendMessage(UM_LC_UPDATE_ROOT, 0, 0);
		return ;
	}
    msg = jsonHelper.CreateJsonString(MESSAGE::DIRINFO, treeCtrlFullPath);
    GetDocument()->GetSocketManager()->Send(msg);

	*pResult = 0;
}

// ## 확장하기 직전 서버에게 fullPath를 보낸다.
void CLeftView::OnTvnItemexpanded(NMHDR *pNMHDR, LRESULT *pResult)
{
    NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
    pNMTreeView = nullptr;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
    CRemoteExplorerDoc* doc = GetDocument();
    CJsonHelper jsonHelper;
    std::string msg;
	
	CString& treeCtrlFullPath = doc->GetTreeCtrlFullPath();

	// ## 루트노드(내 PC)의 path는 서버전송 X
    if(IsRoot(treeCtrlFullPath)) { 
		return ;
	}
    msg = jsonHelper.CreateJsonString(MESSAGE::CHILDDIRINFO, treeCtrlFullPath);
    GetDocument()->GetSocketManager()->Send(msg);
	*pResult = 0;
}

// ## fullPath를 이용하여 서버로부터 받은 자식, 자손들을 컨트롤에 추가.
LRESULT CLeftView::UpdateTreeCtrl(WPARAM /*wparam*/, LPARAM lparam) {

	int iconIndex = GetDocument()->GetFolderImageIndex(0);

	CTreeCtrl& treeCtrl = GetTreeCtrl();
    CRemoteExplorerDoc* doc = GetDocument();
    HTREEITEM selectedItem = treeCtrl.GetSelectedItem();
    std::vector<CChildDirInfo*>* childList = (std::vector<CChildDirInfo*>*)lparam;

	// ## 자식노드 모두 삭제
    DeleteChildAllItems(selectedItem);

    int childSize = childList->size();
    for(int childIndex = 0; childIndex < childSize; childIndex++) {
        CChildDirInfo* child = childList->at(childIndex);
        CString name = child->GetChildName();
        bool isDescendentZero = child->GetIsDescendentZero();

        HTREEITEM childItem = treeCtrl.InsertItem(name, iconIndex, iconIndex, selectedItem);
        if(!isDescendentZero) {
            int dirIcon = doc->GetFolderImageIndex(0);
            treeCtrl.InsertItem(_T(""), dirIcon, dirIcon, childItem);
        }

        if(child != nullptr) {
            delete child;
        }
    }

	return 0;
}

// ## curFullPath를 이용하여 트리 컨트롤 상태 변경.
LRESULT CLeftView::LVRequestProcessor(WPARAM /*wparam*/, LPARAM lparam) {
    int requestType = static_cast<int>(lparam);
	CTreeCtrl& treeCtrl = GetTreeCtrl();

	// ## 현재 선택된 트리 컨트롤 아이템.
	HTREEITEM selectedItem = treeCtrl.GetSelectedItem();
	HTREEITEM rootItem = treeCtrl.GetRootItem();
	HTREEITEM upperItem = treeCtrl.GetParentItem(selectedItem);

	// ## 현재 선택된 트리 컨트롤 아이템 미리 확장.
	treeCtrl.Expand(selectedItem, TVE_EXPAND);
	switch (requestType)
	{
		// ## 이동할 위치가 루트인 경우
	case MOVE_ROOT:
		{
			treeCtrl.SelectItem(rootItem);
			treeCtrl.Expand(selectedItem, TVE_COLLAPSE);
		}
		break;

		// ## 이동할 위치가 상위 폴더인 경우
	case MOVE_UP:
		{
			treeCtrl.SelectItem(upperItem);
			treeCtrl.Expand(selectedItem, TVE_COLLAPSE);
		}
		break;

		// ## 이동할 위치가 하위 폴더인 경우
	case MOVE_DOWN:
		{
			CRemoteExplorerDoc* doc = GetDocument();
			CRemoteExplorerView *listView = doc->GetListView();

			CString selectedItemName = listView->GetSelectedItemName();

			// ## 모든 하위 폴더중에 내가 클릭한 폴더명을 찾아서
			// ## 트리컨트롤을 확장해 주는 부분.
			HTREEITEM lowerItem = treeCtrl.GetChildItem(selectedItem);
			while(lowerItem != NULL) {
				CString childItemName = treeCtrl.GetItemText(lowerItem);
                if(_tcscmp(childItemName, selectedItemName) == 0) {
					treeCtrl.SelectItem(lowerItem);
					treeCtrl.Expand(lowerItem, TVE_EXPAND);
					break;
				}
				lowerItem = treeCtrl.GetNextSiblingItem(lowerItem);
			}
		}
		break;

    default:
        break;
	}

	return 0;
}

bool CLeftView::IsRoot(CString selItem) {

    LPITEMIDLIST item = NULL;
	SHGetSpecialFolderLocation(NULL, CSIDL_DRIVES, &item);

	SHFILEINFO sfi ;
	ZeroMemory(&sfi, sizeof(SHFILEINFO));
    SHGetFileInfo(reinterpret_cast<LPCWSTR>(item), 0, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME);
	DestroyIcon(static_cast<HICON>(sfi.hIcon));

    return (_tcscmp(selItem, sfi.szDisplayName) == 0 
        || _tcscmp(selItem, _T("\\")) == 0 ) ? true : false;
}

bool CLeftView::IsDrive(CString selItem, CString& treeCtrlFullPath) {
	int driveCheck = selItem.Find(':');
    if(driveCheck != -1 && IsRoot(treeCtrlFullPath)) {
        // ## 선택된 아이템이 루트일 경우
		treeCtrlFullPath = selItem.Mid(driveCheck-1, 2) + treeCtrlFullPath;
        return true;
    } else if (driveCheck != -1) {
        // ## 그 외의 경우.
        treeCtrlFullPath = selItem.Mid(driveCheck-1, 2) + _T("\\") + treeCtrlFullPath;
        return true;
    }
    return false;
}

void CLeftView::InitTreeCtrlImageList(CTreeCtrl& treeCtrl) {
    LPITEMIDLIST item = NULL;
    SHGetSpecialFolderLocation(NULL, CSIDL_DESKTOP, &item);

    SHFILEINFO sfi;
    ZeroMemory(&sfi, sizeof(SHFILEINFO));
    smallImageList_.Attach((HIMAGELIST)SHGetFileInfo(reinterpret_cast<LPCWSTR>(item), 0, &sfi, sizeof(sfi), SHGFI_SMALLICON | SHGFI_SYSICONINDEX));
    treeCtrl.SetImageList(&smallImageList_, TVSIL_NORMAL);

    DestroyIcon(static_cast<HICON>(sfi.hIcon));
}

void CLeftView::AddTreeCtrlRootInfo(CTreeCtrl& treeCtrl) {
	LPITEMIDLIST item = NULL;
	SHGetSpecialFolderLocation(NULL, CSIDL_DRIVES, &item);

	SHFILEINFO sfi ;
	ZeroMemory(&sfi, sizeof(SHFILEINFO));
    SHGetFileInfo(reinterpret_cast<LPCWSTR>(item), 0, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_DISPLAYNAME);
	DestroyIcon(static_cast<HICON>(sfi.hIcon));

    hRoot_ = treeCtrl.InsertItem(sfi.szDisplayName, sfi.iIcon, sfi.iIcon, TVI_ROOT);
}

int CLeftView::GetDriveIconIndex(int type, bool isWin) {
    CRemoteExplorerDoc* doc = GetDocument();

    int iconIndex = -1;

    switch (type)
    {
    case DRIVE_FIXED:
        {
            if(isWin) {
                iconIndex = doc->GetWinLocalDriveImageIndex(0);
                break;
            }
            iconIndex = doc->GetLocalDriveImageIndex(0);
        }
        break;

    case DRIVE_CDROM:
        {
            iconIndex = doc->GetCdDriveImageIndex(0);
        }
        break;

    default:
        break;
    }

    return iconIndex;
}

void CLeftView::DeleteChildAllItems(HTREEITEM& selectedItem) {
    CTreeCtrl& treeCtrl = GetTreeCtrl();
    
	if((selectedItem != NULL) && treeCtrl.ItemHasChildren(selectedItem)) {
        HTREEITEM hChild = treeCtrl.GetChildItem(selectedItem);
        while(hChild != nullptr) {
			treeCtrl.DeleteItem( hChild );
            hChild = treeCtrl.GetChildItem(selectedItem);
		}
	}
}
