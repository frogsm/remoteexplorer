﻿
// RemoteExplorerDoc.h : CRemoteExplorerDoc 클래스의 인터페이스

#ifndef REMOTEEXPLORERDOC_H
#define REMOTEEXPLORERDOC_H

#include "LeftView.h"
#include "RemoteExplorerView.h"
#include "SocketManager.h"

#include <vector>
#include <utility>
using namespace std;

class CRemoteExplorerDoc : public CDocument
{
protected: // serialization에서만 만들어집니다.
	CRemoteExplorerDoc();
	DECLARE_DYNCREATE(CRemoteExplorerDoc)

// 특성입니다.
public:

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// 구현입니다.
public:
	virtual ~CRemoteExplorerDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

public:
    

private :
    CSocketManager* socketManager_;

	// ## 원격지의 드라이브 리스트
	vector<pair<CString, int>> *driveList_;

	// ## 트리 컨트롤의 Path
	CString *treeCtrlFullPath_;

	// ## 현재 선택된 Path
	CString *listCtrlFullPath_;

	// ## 폴더 이미지 인덱스
	int folderImgSmall_;
	int folderImgLarge_;

	// ## 윈도우 설치 로컬 드라이브 이미지 인덱스
	int winLocalDriveImgSmall_;
	int winLocalDriveImgLarge_;

	// ## 로컬 드라이브 이미지 인덱스
	int localDriveImgSmall_;
	int localDriveImgLarge_;

	// ## CD 드라이브 이미지 인덱스
	int cdDriveImgSmall_;
	int cdDriveImgLarge_;	

public :
    CSocketManager* GetSocketManager(void) const;
	CString& GetTreeCtrlFullPath(void) const;
	CString& GetListCtrlFullPath(void) const;
	vector<pair<CString, int>>& GetDriveList(void) const;
	
	int GetFolderImageIndex(int size);
	int GetWinLocalDriveImageIndex(int size);
	int GetLocalDriveImageIndex(int size);
	int GetCdDriveImageIndex(int size);

    CLeftView* GetTreeView(void);
	CRemoteExplorerView* GetListView(void);

    // ## 이름과 확장명을 이용하여 타입 가져오기.
	CString GetTypeStr(CString name, CString extension);
    // ## 확장명을 이용하여 아이콘 인덱스 받아오기. (폴더는 _T(" "))
	int GetIconIndex(CString extension);
    
private:
    void SetImageIndex(void) ;
	void SetIconIndex(int& icon, CString path, UINT flag = 0);
    CString GetWinDrive(void);
    CString GetTypeDrive(CString path);
    bool IsWinDrive(CString driveName);
    bool IsLocalDrive(CString driveName);
    bool IsCDDrive(CString driveName);

#ifdef SHARED_HANDLERS
	// 검색 처리기에 대한 검색 콘텐츠를 설정하는 도우미 함수
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};

#endif /*REMOTEEXPLORERDOC_H*/