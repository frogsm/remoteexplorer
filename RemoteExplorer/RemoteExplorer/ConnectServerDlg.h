﻿#ifndef CONNECTSERVERDLG_H
#define CONNECTSERVERDLG_H
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"
#include "MainFrm.h"			// ## CMainFrame 클래스 선언.
#include "RemoteExplorerDoc.h"	// ## CRemoteExplorerDoc 클래스 선언.

// CConnectServerDlg 대화 상자입니다.

class CConnectServerDlg : public CDialog
{
	DECLARE_DYNAMIC(CConnectServerDlg)

public:
	CConnectServerDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CConnectServerDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CONNECTSERVER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

// ## 사용자 데이터 추가
private:
	CRemoteExplorerDoc* doc_;
	CIPAddressCtrl ctrlIPAddress_;
	DWORD valIPAddress_;
	CEdit ctrlPortNum_;
    CString convertIPStr_;
	int valPortNum_;
    

public:
	virtual BOOL OnInitDialog();
    CString GetIPString(void);
    int GetPortNumber(void);
	virtual void OnOK();
	virtual void OnCancel();
	
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);

private:
	// ## 서버 연결
	void ConnectServer(CString ipAddress, int portNum);
	// ## 서버 연결 해제
	void DisconnectServer(void);
};
#endif /*CONNECTSERVERDLG_H*/