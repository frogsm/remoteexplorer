﻿#include "stdafx.h"
#include "SocketManager.h"
#include "JsonHelper.h"

CSocketManager::CSocketManager(void)
{
}

CSocketManager::~CSocketManager(void)
{
    if(clientSocket_ != nullptr) {
        delete clientSocket_;
    }
}

void CSocketManager::InitClientSocket(CString ip, int port) {
    clientSocket_ = new CClientSocket(this);

    clientSocket_->SetIpAddress(ip);
    if(!AfxSocketInit()) {
        AfxMessageBox(ID_SOCKET_INIT);
        AfxGetMainWnd()->PostMessage(WM_CLOSE);
    }
    if(!clientSocket_->Create()) {
        AfxMessageBox(ID_SOCKET_CREATE);
        AfxGetMainWnd()->PostMessage(WM_CLOSE);
    }
    
    clientSocket_->Connect(ip, port);
}

void CSocketManager::TerminateClientSocket(void) {
    clientSocket_->Close();
}

void CSocketManager::Connect(void) {
    return ;
}

void CSocketManager::Accept(void) {
    return ;
}

void CSocketManager::Send(std::string msg) {
    clientSocket_->Send(msg.c_str(), msg.length() + 1);
}

void CSocketManager::Receive(CString msg) {
    CJsonHelper jsonHelper;
    jsonHelper.ParseJsonString(msg);
}

void CSocketManager::Close(void) {
    AfxMessageBox(ID_SOCKET_CLOSE);
    AfxGetMainWnd()->PostMessage(WM_CLOSE);
}
