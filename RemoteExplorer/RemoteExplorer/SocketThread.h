﻿#ifndef SOCKETTHREAD_H
#define SOCKETTHREAD_H

#include "ClientSocket.h"

class AFX_EXT_CLASS CSocketThread : public CWinThread
{
    DECLARE_DYNCREATE(CSocketThread)
private:
    CClientSocket* clientSocket_;
    SOCKET socket_;

public:
    CSocketThread(void);
    CSocketThread(CClientSocket* client, SOCKET soket);
    virtual ~CSocketThread(void);
    virtual BOOL InitInstance();
    virtual int ExitInstance();
};

#endif /*SOCKETTHREAD_H*/