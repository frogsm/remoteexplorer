﻿#include "stdafx.h"
#include "ClientSocket.h"
#include "resource.h"


CClientSocket::CClientSocket(void)
{
}

CClientSocket::CClientSocket(CBaseManager* baseManager) {
    baseManager_ = baseManager;
}

CClientSocket::~CClientSocket(void)
{
}

void CClientSocket::SetIpAddress(CString ip) {
    ipAddress_ = ip;
}

void CClientSocket::SetIpAddress(char* ip) {
    CString temp(ip);
    this->ipAddress_ = temp;
}

CString CClientSocket::GetIpAddress(void) const {
    return ipAddress_;
}

void CClientSocket::OnConnect(int nErrorCode) {
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    if(nErrorCode == WSAECONNREFUSED) {
        AfxMessageBox(ID_SOCKET_CONNECT);
        AfxGetMainWnd() -> PostMessage(WM_CLOSE);
    }
    CAsyncSocket::OnConnect(nErrorCode);
}

void CClientSocket::OnReceive(int nErrorCode) {
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    char buff[4096];
    ZeroMemory(buff, 4096);
    CString recvStr = _T("");

    int nRead = 0;
    while((nRead = Receive(buff, 4096)) > 0) {
        CString temp = CString(buff, nRead);
        recvStr += temp;
    }
    baseManager_->Receive(recvStr);

    CAsyncSocket::OnReceive(nErrorCode);
}

void CClientSocket::OnClose(int nErrorCode) {
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    baseManager_->Close();

	CAsyncSocket::OnClose(nErrorCode);
}
