//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// Socket.rc에서 사용되고 있습니다.
//
#define IDP_SOCKETS_INIT_FAILED         101
#define ID_SOCKET_INIT                  1000
#define ID_SOCKET_CREATE                1001
#define ID_SOCKET_CONNECT               1002
#define ID_SOCKET_LISTEN                1003
#define ID_SOCKET_ACCEPT                1004
#define ID_SOCKET_CLOSE                 1005
#define ID_SOCKET_EXIT                  1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7001
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         7000
#define _APS_NEXT_SYMED_VALUE           7000
#endif
#endif
