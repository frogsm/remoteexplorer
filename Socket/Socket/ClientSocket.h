﻿#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include "afxsock.h"
#include "BaseManager.h"

class AFX_EXT_CLASS CClientSocket : public CAsyncSocket
{
private:
    CBaseManager* baseManager_;
    CString ipAddress_;

    CClientSocket(void);

public:
    CClientSocket(CBaseManager* baseManager);
    virtual ~CClientSocket(void);

    virtual void SetIpAddress(CString ip);
    virtual void SetIpAddress(char* ip);
    virtual CString GetIpAddress(void) const;

    virtual void OnConnect(int nErrorCode);
    virtual void OnReceive(int nErrorCode);
    virtual void OnClose(int nErrorCode);
};

#endif /*CLIENTSOCKET_H*/