﻿#include "stdafx.h"
#include "ThreadManager.h"


CThreadManager::CThreadManager(void)
{
}


CThreadManager::~CThreadManager(void)
{
}

CSocketThread* CThreadManager::CreateSocketThread(CClientSocket* socket) {
    CSocketThread* thread = new CSocketThread(socket, socket->Detach());
    thread->CreateThread();

    return thread;
}

void CThreadManager::DeleteSocketThread(CSocketThread* thread) {
    StopThread(thread);
    while(IsArrive(thread)) {
        WaitForSingleObject(thread->m_hThread, 500);
    }
    delete thread;
}

void CThreadManager::StopThread(CSocketThread* thread) {
    thread->PostThreadMessage(WM_QUIT, NULL, NULL);
}

bool CThreadManager::IsArrive(CSocketThread* thread) {
    DWORD dwExitCode;
    GetExitCodeThread(thread->m_hThread, &dwExitCode);

    return dwExitCode == STILL_ACTIVE ? true : false;
}

