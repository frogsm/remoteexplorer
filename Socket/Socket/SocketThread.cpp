﻿#include "stdafx.h"
#include "SocketThread.h"

IMPLEMENT_DYNCREATE(CSocketThread, CWinThread);

class CAsyncSocket;

CSocketThread::CSocketThread(void) {

}

CSocketThread::CSocketThread(CClientSocket* client, SOCKET soket) 
    : clientSocket_(client), socket_(soket)
{
    m_bAutoDelete = false;
}

CSocketThread::~CSocketThread(void)
{
}

BOOL CSocketThread::InitInstance()
{
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    AfxSocketInit();
    clientSocket_->Attach(socket_);

    return true;
}

int CSocketThread::ExitInstance()
{
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    delete clientSocket_;

    return CWinThread::ExitInstance();
}