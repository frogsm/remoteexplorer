﻿#include "stdafx.h"
#include "ListenSocket.h"
#include "resource.h"


CListenSocket::CListenSocket(void)
{
}

CListenSocket::CListenSocket(CBaseManager* baseManager) {
    baseManager_ = baseManager;
}

CListenSocket::~CListenSocket(void)
{
}

void CListenSocket::OnAccept(int nErrorCode)
{
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    baseManager_->Accept();
    
    CAsyncSocket::OnAccept(nErrorCode);
}